import os
from flask import g, Flask
from flask_cors import CORS
from flask_restful import Api, Resource
from flask_restful.reqparse import RequestParser
from urllib.parse import urlencode
from uuid import uuid4
from functools import wraps

from api_scraping.db_setup import create_supabase_client


def get_supabase():
    if "supabase" not in g:
        g.supabase = create_supabase_client()
    return g.supabase


def generate_map_url(resource):
    if not resource.get("borough"):
        return None

    params = {
        "key": os.environ.get("GOOGLE_API_KEY"),
        "q": (
            f"{resource['address']}, {resource['borough']}"
            if resource["address"]
            else resource["borough"]
        ),
    }
    return f"https://www.google.com/maps/embed/v1/place?{urlencode(params)}"


def build_query(table_name, args, supabase):
    query = supabase.table(table_name).select("*")
    for arg, value in args.items():
        if value:
            if arg in [
                "borough",
                "wheelchair_access",
                "faith_based",
                "nonprofit",
                "child_friendly",
                "lgbtq_specialized",
            ]:
                query = query.eq(arg, value)
            elif arg == "language_svcs":
                query = query.contains("language_svcs", [value])

    if args.get("name_order"):
        query = query.order("name", desc=(args["name_order"] == "desc"))
    return query


def parse_args(required_args):
    parser = RequestParser()
    for arg, arg_type in required_args.items():
        parser.add_argument(arg, type=arg_type, location="args", required=arg in ["id"])
    return parser.parse_args()


def supabase_client(f):
    @wraps(f)
    def decorated_function(self, *args, **kwargs):
        supabase = get_supabase()
        return f(self, supabase, *args, **kwargs)

    return decorated_function


class ResourceBase(Resource):
    required_args = {}

    @supabase_client
    def get(self, supabase):
        args = parse_args(self.required_args)
        query = build_query(self.table_name, args, supabase)
        results = query.execute()
        if not results.data:
            return {"error": "Resource not found"}, 404
        return {
            "type": self.__class__.__name__,
            "response_uuid": str(uuid4()),
            "resources": results.data,
        }


class EducationalResource(Resource):
    def get(self):
        supabase = get_supabase()
        parser = RequestParser()
        parser.add_argument("id", type=int, location="args")
        id_ = parser.parse_args()["id"]

        resource_data = (
            supabase.table("educational_resources").select("*").eq("id", id_).execute()
        )
        resource = resource_data.data[0] if resource_data.data else None
        if not resource:
            return {"error": "Resource not found"}, 404

        legal_association = (
            supabase.table("legal_educational")
            .select("*")
            .eq("educational_resource_id", id_)
            .limit(1)
            .execute()
            .data[0]
        )
        physical_association = (
            supabase.table("educational_physical")
            .select("*")
            .eq("educational_resource_id", id_)
            .limit(1)
            .execute()
            .data[0]
        )

        legal_resource = (
            supabase.table("legal_resources")
            .select("*")
            .eq("id", legal_association["legal_resource_id"])
            .execute()
            .data[0]
        )
        physical_resource = (
            supabase.table("physical_resources")
            .select("*")
            .eq("id", physical_association["physical_resource_id"])
            .execute()
            .data[0]
        )

        resource["map_url"] = generate_map_url(resource)

        return {
            "type": self.__class__.__name__,
            "response_uuid": str(uuid4()),
            "resource": resource,
            "associations": {
                "legal": legal_resource,
                "physical": physical_resource,
            },
        }




class EducationalResourceCollection(ResourceBase):
    table_name = "educational_resources"
    required_args = {
        "name_order": str,
        "borough": str,
        "faith_based": str,
        "nonprofit": str,
        "child_friendly": str,
        "lgbtq_specialized": str,
    }


class LegalResource(Resource):
    def get(self):
        supabase = get_supabase()
        parser = RequestParser()
        parser.add_argument("id", type=int, location="args")
        id_ = parser.parse_args()["id"]

        resource_data = (
            supabase.table("legal_resources").select("*").eq("id", id_).execute()
        )
        resource = resource_data.data[0] if resource_data.data else None
        if not resource:
            return {"error": "Resource not found"}, 404

        educational_association = (
            supabase.table("legal_educational")
            .select("*")
            .eq("legal_resource_id", id_)
            .limit(1)
            .execute()
            .data[0]
        )
        physical_association = (
            supabase.table("legal_physical")
            .select("*")
            .eq("legal_resource_id", id_)
            .limit(1)
            .execute()
            .data[0]
        )

        educational_resource = (
            supabase.table("educational_resources")
            .select("*")
            .eq("id", educational_association["educational_resource_id"])
            .execute()
            .data[0]
        )
        physical_resource = (
            supabase.table("physical_resources")
            .select("*")
            .eq("id", physical_association["physical_resource_id"])
            .execute()
            .data[0]
        )

        resource["map_url"] = generate_map_url(resource)

        return {
            "type": self.__class__.__name__,
            "response_uuid": str(uuid4()),
            "resource": resource,
            "associations": {
                "educational": educational_resource,
                "physical": physical_resource,
            },
        }



class LegalResourceCollection(ResourceBase):
    table_name = "legal_resources"
    required_args = {
        "name_order": str,
        "borough": str,
        "faith_based": str,
        "nonprofit": str,
        "child_friendly": str,
        "lgbtq_specialized": str,
    }


class PhysicalResource(Resource):
    def get(self):
        supabase = get_supabase()
        parser = RequestParser()
        parser.add_argument("id", type=int, location="args")
        id_ = parser.parse_args()["id"]

        resource_data = (
            supabase.table("physical_resources").select("*").eq("id", id_).execute()
        )
        resource = resource_data.data[0] if resource_data.data else None
        if not resource:
            return {"error": "Resource not found"}, 404

        legal_association = (
            supabase.table("legal_physical")
            .select("*")
            .eq("physical_resource_id", id_)
            .execute()
            .data[0]
        )
        educational_association = (
            supabase.table("educational_physical")
            .select("*")
            .eq("physical_resource_id", id_)
            .execute()
            .data[-1]
        )

        educational_resource = (
            supabase.table("educational_resources")
            .select("*")
            .eq("id", educational_association["educational_resource_id"])
            .execute()
            .data[0]
        )
        legal_resource = (
            supabase.table("legal_resources")
            .select("*")
            .eq("id", legal_association["legal_resource_id"])
            .execute()
            .data[0]
        )

        resource["map_url"] = generate_map_url(resource)

        return {
            "type": self.__class__.__name__,
            "response_uuid": str(uuid4()),
            "resource": resource,
            "associations": {
                "educational": educational_resource,
                "legal": legal_resource,
            },
        }



class PhysicalResourceCollection(ResourceBase):
    table_name = "physical_resources"
    required_args = {
        "name_order": str,
        "borough": str,
        "wheelchair_access": str,
        "language_svcs": str,
        "child_friendly": str,
        "lgbtq_specialized": str,
    }


def create_app():
    app = Flask(__name__)
    CORS(app)
    api = Api(app)
    api.add_resource(EducationalResource, "/educational/resource")
    api.add_resource(EducationalResourceCollection, "/educational/collection")
    api.add_resource(LegalResource, "/legal/resource")
    api.add_resource(LegalResourceCollection, "/legal/collection")
    api.add_resource(PhysicalResource, "/physical/resource")
    api.add_resource(PhysicalResourceCollection, "/physical/collection")
    return app


if __name__ == "__main__":
    create_app().run(debug=os.environ.get("DEBUG", False))
