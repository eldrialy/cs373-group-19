from api_helper import fetch_data
from api_helper import format_borough

API_URL = "https://data.cityofnewyork.us/resource/5ziv-wcy4.json"


def process_resource_services(resource):
    """Process and services provided by resource"""
    specialized_services = (resource.get("specialized_svcs") or "").split(";")
    services_provided = (resource.get("svcs_provided") or "").split(";")
    relevant_services = [
        "Survivors of Sexual Assault and DV",
        "Abusive Partners",
        "Children at Risk of Abuse/Neglect",
        "Child Witnesses to DV",
        "General Domestic Violence Services",
        "Sexual Assault Services",
    ]
    child_services = ["Children at Risk of Abuse/Neglect", "Child Witnesses to DV"]

    # check if relevant domestic violence services are provided
    if any(
        service in specialized_services + services_provided
        for service in relevant_services
    ):
        child_friendly = any(
            service in specialized_services + services_provided
            for service in child_services
        )
        lgbtq_specialized = "LGBTQ" in specialized_services
        return child_friendly, lgbtq_specialized
    else:
        return None, None


def create_resource_entry(resource):
    """Create dictionary for given resource"""

    # skip resources missing important information
    if (
        not resource.get("longitude")
        or not resource.get("website")
        or not resource.get("language_svcs")
    ):
        return None

    borough = format_borough(resource.get("borough"))
    child_friendly, lgbtq_specialized = process_resource_services(resource)
    # doesn't provide relevant domestic violence service
    if child_friendly is None:
        return None

    return {
        "longitude": resource["longitude"],
        "latitude": resource["latitude"],
        "address": resource.get("address"),
        "borough": borough.strip().capitalize(),
        "name": resource.get("name"),
        "website": resource["website"],
        "language_svcs": resource["language_svcs"].split(";"),
        "wheelchair_access": resource.get("wheelchair_access") == "Yes",
        "child_friendly": child_friendly,
        "lgbtq_specialized": lgbtq_specialized,
        "img_url": "",
        "map_url": "",
    }


def process_mayors_resources():
    """
    Fetch and process data from Mayor’s Office to End Domestic
    and Gender-Based Violence Resource Directory
    """
    try:
        data = fetch_data(API_URL)
        resources = []
        physical_id = 0
        for resource in data:
            resource_entry = create_resource_entry(resource)
            if resource_entry:
                resource_entry["id"] = physical_id
                physical_id += 1
                resources.append(resource_entry)
        return resources
    except Exception as e:
        print(e)
