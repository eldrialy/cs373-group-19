from sqlalchemy import (
    Column,
    Integer,
    String,
    Float,
    Boolean,
    Table,
    ForeignKey,
)
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship


Base = declarative_base()


legal_educational_association = Table(
    "legal_educational",
    Base.metadata,
    Column("legal_resource_id", Integer, ForeignKey("legal_resources.id")),
    Column("educational_resource_id", Integer, ForeignKey("educational_resources.id")),
)

legal_physical_association = Table(
    "legal_physical",
    Base.metadata,
    Column("legal_resource_id", Integer, ForeignKey("legal_resources.id")),
    Column("physical_resource_id", Integer, ForeignKey("physical_resources.id")),
)

educational_physical_association = Table(
    "educational_physical",
    Base.metadata,
    Column("educational_resource_id", Integer, ForeignKey("educational_resources.id")),
    Column("physical_resource_id", Integer, ForeignKey("physical_resources.id")),
)


class LegalResource(Base):
    __tablename__ = "legal_resources"
    id = Column(Integer, primary_key=True)
    website = Column(String)
    address = Column(String)
    name = Column(String)
    borough = Column(String)
    longitude = Column(Float)
    latitude = Column(Float)
    description = Column(String)
    faith_based = Column(Boolean)
    nonprofit = Column(Boolean)
    child_friendly = Column(Boolean)
    lgbtq_specialized = Column(Boolean)
    img_url = Column(String)
    map_url = Column(String)
    educational_resources = relationship(
        "EducationalResource",
        secondary=legal_educational_association,
        back_populates="legal_resources",
    )
    physical_resources = relationship(
        "PhysicalResource",
        secondary=legal_physical_association,
        back_populates="legal_resources",
    )

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class EducationalResource(Base):
    __tablename__ = "educational_resources"
    id = Column(Integer, primary_key=True)
    website = Column(String)
    address = Column(String)
    name = Column(String)
    borough = Column(String)
    longitude = Column(Float)
    latitude = Column(Float)
    description = Column(String)
    faith_based = Column(Boolean)
    nonprofit = Column(Boolean)
    child_friendly = Column(Boolean)
    lgbtq_specialized = Column(Boolean)
    img_url = Column(String)
    map_url = Column(String)
    legal_resources = relationship(
        "LegalResource",
        secondary=legal_educational_association,
        back_populates="educational_resources",
    )
    physical_resources = relationship(
        "PhysicalResource",
        secondary=educational_physical_association,
        back_populates="educational_resources",
    )

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class PhysicalResource(Base):
    __tablename__ = "physical_resources"
    id = Column(Integer, primary_key=True)
    website = Column(String)
    address = Column(String)
    name = Column(String)
    borough = Column(String)
    longitude = Column(Float)
    latitude = Column(Float)
    child_friendly = Column(Boolean)
    lgbtq_specialized = Column(Boolean)
    wheelchair_access = Column(Boolean)
    language_svcs = Column(ARRAY(String))
    img_url = Column(String)
    map_url = Column(String)
    educational_resources = relationship(
        "EducationalResource",
        secondary=educational_physical_association,
        back_populates="physical_resources",
    )
    legal_resources = relationship(
        "LegalResource",
        secondary=legal_physical_association,
        back_populates="physical_resources",
    )

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
