from api_helper import fetch_data
from api_helper import format_borough

API_URL = "https://data.cityofnewyork.us/resource/pqg4-dm6b.json"


def determine_borough(resource):
    """Determine and format borough"""
    borough_keys = [
        ("brooklyn", "Brooklyn"),
        ("bronx", "Bronx"),
        ("manhattan", "Manhattan"),
        ("queens", "Queens"),
        ("staten_island", "Staten Island"),
    ]

    for key, name in borough_keys:
        if resource.get(key) == "Y":
            return key
    return format_borough(resource.get("borough", ""))


def clean_description(description):
    """Clean and format the description"""
    return description.replace("\r", "").replace("\n", "") if description else ""


def is_resource_qualified(resource):
    """Check if resource relates to domestic violence"""
    return (
        resource.get("domestic_violence") == "Y"
        or resource.get("victim_services") == "Y"
    )


def assemble_resource_data(resource):
    """Assemble dictionary from data"""
    return {
        "website": resource.get("url"),
        "address": resource.get("address1"),
        "name": resource.get("organizationname"),
        "borough": determine_borough(resource),
        "longitude": resource.get("longitude"),
        "latitude": resource.get("latitude"),
        "description": clean_description(resource.get("description")),
        "faith_based": resource.get("faith_based_organization") == "Y",
        "nonprofit": resource.get("nonprofit") == "Y",
        "child_friendly": resource.get("child_care_parent_information") == "Y"
        or resource.get("youth_services") == "Y",
        "lgbtq_specialized": resource.get("lesbian_gay_bisexual_and_or_transgender")
        == "Y",
        "img_url": "",
        "map_url": "",
    }


def is_legal_service(resource):
    """Determine if resource provides legal services"""
    return resource.get("legal_services") == "Y" or resource.get("immigration") == "Y"


def is_educational_service(resource):
    """Determine if resource provides educational services"""
    educational_keys = [
        "counseling_support_groups",
        "community_service_volunteerism",
        "employment_job_training",
        "personal_finance_financial_education",
    ]
    return resource.get("education") == "Y" or any(
        resource.get(key) == "Y" for key in educational_keys
    )


def process_women_resources():
    """
    Fetch and process resources from NYC Women's Resource Network Database
    """
    try:
        data = fetch_data(API_URL)
        legal_resources = []
        educational_resources = []
        legal_id = 0
        education_id = 0

        for resource in data:
            if is_resource_qualified(resource):
                resource_data = assemble_resource_data(resource)

                if is_legal_service(resource):
                    legal_resource_data = resource_data.copy()
                    legal_resource_data["id"] = legal_id
                    legal_id += 1
                    legal_resources.append(legal_resource_data)
                if is_educational_service(resource):
                    educational_resource_data = resource_data.copy()
                    educational_resource_data["id"] = education_id
                    education_id += 1
                    educational_resources.append(educational_resource_data)

        return legal_resources, educational_resources
    except Exception as e:
        print(e)
