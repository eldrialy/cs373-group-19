import requests

STATUS_OK = 200


def fetch_data(api_url):
    """Fetch data from the API."""
    response = requests.get(api_url)
    if response.status_code != STATUS_OK:
        raise Exception(
            f"Failed to fetch data from API. Status code: {response.status_code}"
        )
    return response.json()


def format_borough(borough):
    """Format the borough name to be capitalized and trimmed."""
    return borough.strip().lower().capitalize() if borough else None
