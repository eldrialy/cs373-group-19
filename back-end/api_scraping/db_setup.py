from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from api_scraping.db_models import Base
from supabase import create_client

DATABASE_URL = "postgresql://postgres.nzublddmgwmimdtkbrqd:nycsurvivorsupport!@aws-0-us-west-1.pooler.supabase.com:5432/postgres"
SUPABASE_URL = "https://nzublddmgwmimdtkbrqd.supabase.co"
SUPABASE_KEY = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Im56dWJsZGRtZ3dtaW1kdGticnFkIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDkwMDUyNjcsImV4cCI6MjAyNDU4MTI2N30.J4QiFKIqizbuIrMabjhgsH0cw_gawyfCE5Lb_udusyE"


def create_db_session():
    """Sets up db connection to supabase"""
    engine = create_engine(DATABASE_URL)
    session = sessionmaker(bind=engine)
    Base.metadata.create_all(engine)
    return session()


def create_supabase_client():
    """Sets up the Supabase client."""
    supabase = create_client(SUPABASE_URL, SUPABASE_KEY)
    return supabase
