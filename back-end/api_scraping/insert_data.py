from db_models import (
    LegalResource,
    EducationalResource,
    PhysicalResource,
    educational_physical_association,
    legal_physical_association,
    legal_educational_association,
)
from db_setup import create_db_session
from scrape_women_api import process_women_resources
from scrape_mayor_api import process_mayors_resources
from math import radians, cos, sin, sqrt, atan2


def insert_resources(session, model, resources):
    """
    Inserts resources into the db based on the model
    """
    try:
        session.bulk_insert_mappings(model, resources)
        session.commit()
        print(
            f"Successfully inserted {len(resources)} resources into {model.__tablename__}."
        )
    except Exception as e:
        session.rollback()
        print(f"Error inserting into {model.__tablename__}: {e}")


def calculate_distance(lat1, lon1, lat2, lon2):
    """
    Calculates the distance between the two resources
    """
    if not lat1 or not lon1 or not lat2 or not lon2:
        return None
    R = 6371.0

    dlat = radians(lat2 - lat1)
    dlon = radians(lon2 - lon1)
    a = (
        sin(dlat / 2) ** 2
        + cos(radians(lat1)) * cos(radians(lat2)) * sin(dlon / 2) ** 2
    )
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c
    return distance


def find_closest_resource(source_resource, target_resources):
    """
    Iterates through each of the target resources to find the closest one
    to the current source resource
    """
    closest_resource = None
    min_distance = float("inf")
    for target_resource in target_resources:
        if closest_resource is None:
            closest_resource = target_resource
        if source_resource.name.lower() != target_resource.name.lower():
            if source_resource.latitude and source_resource.longitude:
                distance = calculate_distance(
                    source_resource.latitude,
                    source_resource.longitude,
                    target_resource.latitude,
                    target_resource.longitude,
                )
                if distance and distance < min_distance:
                    min_distance = distance
                    closest_resource = target_resource
            # fall back to closest borough if source lacks longitude/latitude
            elif source_resource.borough.lower() == target_resource.borough.lower():
                closest_resource = target_resource
                break
    return closest_resource


def update_association(session, source_resource, target_resource, association_table):
    """
    Updates all of the association tables
    """
    try:
        source_type = type(source_resource).__name__
        target_type = type(target_resource).__name__

        insert_values = {}

        type_column_map = {
            "LegalResource": "legal_resource_id",
            "EducationalResource": "educational_resource_id",
            "PhysicalResource": "physical_resource_id",
        }

        if source_type in type_column_map and target_type in type_column_map:
            insert_values[type_column_map[source_type]] = source_resource.id
            insert_values[type_column_map[target_type]] = target_resource.id
        else:
            raise ValueError(f"Unexpected resource types: {source_type}, {target_type}")

        # check for existing association to avoid duplicates
        exists = session.query(association_table).filter_by(**insert_values).first()
        if not exists:
            session.execute(association_table.insert().values(**insert_values))
            session.commit()
            print(
                f"Association created between {source_type} ID {source_resource.id} and {target_type} ID {target_resource.id} in {association_table.name}."
            )
        else:
            print(
                f"Association already exists between {source_type} ID {source_resource.id} and {target_type} ID {target_resource.id} in {association_table.name}."
            )

    except Exception as e:
        session.rollback()
        print(f"Error updating association in {association_table.name}: {e}")


def update_resource_associations(session):
    """
    Find the closest resources from the other models and update
    the corresponding association tables
    """
    try:
        legal_resources = session.query(LegalResource).all()
        educational_resources = session.query(EducationalResource).all()
        physical_resources = session.query(PhysicalResource).all()

        for resource_list, assoc1, assoc2 in [
            (
                legal_resources,
                legal_educational_association,
                legal_physical_association,
            ),
            (
                educational_resources,
                legal_educational_association,
                educational_physical_association,
            ),
            (
                physical_resources,
                legal_physical_association,
                educational_physical_association,
            ),
        ]:
            for resource in resource_list:
                if isinstance(resource, LegalResource):
                    closest_educational = find_closest_resource(
                        resource, educational_resources
                    )
                    closest_physical = find_closest_resource(
                        resource, physical_resources
                    )
                    update_association(
                        session,
                        resource,
                        closest_educational,
                        legal_educational_association,
                    )
                    update_association(
                        session, resource, closest_physical, legal_physical_association
                    )
                elif isinstance(resource, EducationalResource):
                    closest_legal = find_closest_resource(resource, legal_resources)
                    closest_physical = find_closest_resource(
                        resource, physical_resources
                    )
                    update_association(
                        session, resource, closest_legal, legal_educational_association
                    )
                    update_association(
                        session,
                        resource,
                        closest_physical,
                        educational_physical_association,
                    )
                elif isinstance(resource, PhysicalResource):
                    closest_legal = find_closest_resource(resource, legal_resources)
                    closest_educational = find_closest_resource(
                        resource, educational_resources
                    )
                    update_association(
                        session, resource, closest_legal, legal_physical_association
                    )
                    update_association(
                        session,
                        resource,
                        closest_educational,
                        educational_physical_association,
                    )

        session.commit()
        print("Resource associations updated successfully.")
    except Exception as e:
        session.rollback()
        print(f"Failed to update resource associations: {e}")


def main():
    """Fetch and insert data from APIs into db"""
    session = create_db_session()

    try:
        legal, educational = process_women_resources()
        insert_resources(session, LegalResource, legal)
        insert_resources(session, EducationalResource, educational)

    except Exception as e:
        print(f"Error processing women resources: {e}")

    try:
        physical = process_mayors_resources()
        insert_resources(session, PhysicalResource, physical)

    except Exception as e:
        print(f"Error processing mayors resources: {e}")

    update_resource_associations(session)

    session.close()


if __name__ == "__main__":
    main()
