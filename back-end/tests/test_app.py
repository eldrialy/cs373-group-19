def test_educational_resource(client):
    resource_id = 0
    response = client.get(f"/educational/resource?id={resource_id}")
    assert response.json["type"] == "EducationalResource"
    assert response.json["resource"]["id"] == resource_id
    assert response.json["associations"]["legal"]
    assert response.json["associations"]["physical"]


def test_educational_resource_collection(client):
    response = client.get(f"/educational/collection")
    assert response.json["type"] == "EducationalResourceCollection"
    assert response.json["resources"]
    assert "id" in response.json["resources"][0]


def test_legal_resource(client):
    resource_id = 0
    response = client.get(f"/legal/resource?id={resource_id}")
    assert response.json["type"] == "LegalResource"
    assert response.json["resource"]["id"] == resource_id
    assert response.json["associations"]["educational"]
    assert response.json["associations"]["physical"]


def test_legal_resource_collection(client):
    response = client.get(f"/legal/collection")
    assert response.json["type"] == "LegalResourceCollection"
    assert response.json["resources"]
    assert "id" in response.json["resources"][0]


def test_physical_resource(client):
    resource_id = 0
    response = client.get(f"/physical/resource?id={resource_id}")
    assert response.json["type"] == "PhysicalResource"
    assert response.json["resource"]["id"] == resource_id
    assert response.json["associations"]["educational"]
    assert response.json["associations"]["legal"]


def test_physical_resource_collection(client):
    response = client.get(f"/physical/collection")
    assert response.json["type"] == "PhysicalResourceCollection"
    assert response.json["resources"]
    assert "id" in response.json["resources"][0]


def test_educational_resource_collection_filter(client):
    response = client.get(
        f"/educational/collection?borough=Manhattan&nonprofit=true&faith_based=false&child_friendly=false"
    )
    assert response.json["type"] == "EducationalResourceCollection"
    assert response.json["resources"]
    for i, resource in enumerate(response.json["resources"]):
        assert resource["borough"] == "Manhattan"
        assert resource["nonprofit"]
        assert not resource["faith_based"]
        assert not resource["child_friendly"]
        if i:
            assert resource["name"] >= response.json["resources"][i - 1]["name"]


def test_legal_resource_collection_filter(client):
    response = client.get(f"/legal/collection?name_order=desc&lgbtq_specialized=true")
    assert response.json["type"] == "LegalResourceCollection"
    assert response.json["resources"]
    for i, resource in enumerate(response.json["resources"]):
        assert resource["lgbtq_specialized"]
        if i:
            assert resource["name"] <= response.json["resources"][i - 1]["name"]


def test_physical_resource_collection_filter(client):
    response = client.get(
        f"/physical/collection?wheelchair_access=true&language_svcs=Spanish"
    )
    assert response.json["type"] == "PhysicalResourceCollection"
    assert response.json["resources"]
    for i, resource in enumerate(response.json["resources"]):
        assert resource["wheelchair_access"]
        assert "Spanish" in resource["language_svcs"]
        if i:
            assert resource["name"] >= response.json["resources"][i - 1]["name"]
