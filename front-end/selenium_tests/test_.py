import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

URL = "https://www.nycsurvivorsupport.me/"

class TestSite(unittest.TestCase):
  def setUp(self) -> None:
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument("window-size=2560x1440")
    self.driver = webdriver.Chrome(options=options)
    self.driver.implicitly_wait(10)
    self.driver.get(URL)
    return super().setUp()
    
  def tearDown(self) -> None:
    self.driver.quit()
    return super().tearDown()

  # Test that splash page renders text as expected
  def test_SplashTitle(self):
    self.driver.get(URL)
    element = self.driver.find_element(By.XPATH, "//h1[contains(text(), 'NYC Survivor Support')]")

    actual = element.text
    expected = "NYC Survivor Support"
    self.assertEqual(actual, expected)

  # Test that splash page renders text as expected
  def test_SplashInfo(self):
    self.driver.get(URL)
    element = self.driver.find_element(By.CLASS_NAME, "intro-paragraph")

    actual = element.text
    expected = "NYC Survivor Support aims to bring awareness to survivors of domestic violence. Not only is this an educational site, but it is also useful for resources for both victims and those who wish to offer support. The categories below will provide information about legal resources for victims, and also physical resources for both victims and supporters alike."
    self.assertEqual(actual, expected)

  # Test that about page renders site info as expected
  def test_AboutUs(self):
    self.driver.get(URL + "about")
    # element = self.driver.find_element(By.NAME, "site_info")
    element = self.driver.find_element(By.XPATH, "//h1[contains(text(), 'About Us')]")

    actual = element.text
    expected = "About Us"
    self.assertEqual(actual, expected)

  # Test that about page renders team info as expected
  def test_AboutTeam(self):
    self.driver.get(URL + "about")
   # element = self.driver.find_element(By.NAME, "team_info")
    element = self.driver.find_element(By.XPATH, "//h2[contains(text(), 'Our Team')]")
    
    actual = element.text
    expected = "Our Team"
    self.assertEqual(actual, expected)
      
  # Test that the navbar button for Home displays the correct text and routes to the correct page
  def test_NavBarHome(self):
    self.driver.get(URL)
    nav_brand  = self.driver.find_element(By.XPATH, "//a[@class='navbar-brand'][@href='/']")
    actual = nav_brand.text
    expected = "NYC Survivor Support"
    self.assertEqual(actual, expected)

    nav_home  = self.driver.find_element(By.XPATH, "//a[@class='nav-link'][@href='/']")
    actual = nav_home.text
    expected = "Home"
    self.assertEqual(actual, expected)

    nav_home.click()
    WebDriverWait(self.driver, 10)
    
    actual = self.driver.current_url
    self.assertEqual(actual, URL)

  # Test that the navbar button for About displays the correct text and routes to the correct page
  def test_NavBarAbout(self):
    self.driver.get(URL)
    nav_about = self.driver.find_element(By.XPATH, "//a[@href='/about']")
    actual = nav_about.text
    expected = "About"
    self.assertEqual(actual, expected)

    nav_about.click()
    WebDriverWait(self.driver, 10)

    actual = self.driver.current_url
    expected = URL + "about"
    self.assertEqual(actual, expected)

  # Test that the navbar button for Education displays the correct text and routes to the correct page
  def test_NavBarEdu(self):
    self.driver.get(URL)
    nav_edu   = self.driver.find_element(By.XPATH, "//a[@href='/education']")
    actual = nav_edu.text
    expected = "Get Educated"
    self.assertEqual(actual, expected)

    nav_edu.click()
    WebDriverWait(self.driver, 10)

    actual = self.driver.current_url
    expected = URL + "education"
    self.assertEqual(actual, expected)

  # Test that the navbar button for Legal displays the correct text and routes to the correct page
  def test_NavBarLegal(self):
    self.driver.get(URL)
    nav_legal = self.driver.find_element(By.XPATH, "//a[@href='/legal']")
    actual = nav_legal.text
    expected = "Legal Assistance"
    self.assertEqual(actual, expected)

    nav_legal.click()
    WebDriverWait(self.driver, 10)

    actual = self.driver.current_url
    expected = URL + "legal"
    self.assertEqual(actual, expected)

  # Test that the navbar button for Physical displays the correct text and routes to the correct page
  def test_NavBarPhys(self):
    self.driver.get(URL)
    nav_phys  = self.driver.find_element(By.XPATH, "//a[@href='/physical']")
    actual = nav_phys.text
    expected = "Find Physical Resources"
    self.assertEqual(actual, expected)

    nav_phys.click()
    WebDriverWait(self.driver, 10)

    actual = self.driver.current_url
    expected = URL + "physical"
    self.assertEqual(actual, expected)

  # Test that Education model page loads as expected
  def test_EduModelPage(self):
    self.driver.get(URL + "education")
    element = self.driver.find_element(By.XPATH, "//*[@id='root']/div/header/h1")
    actual = element.text
    expected = "Education Resources"
    self.assertEqual(actual, expected)

  # Test that Legal model page loads as expected
  def test_LegalModelPage(self):
    self.driver.get(URL + "legal")
    element = self.driver.find_element(By.XPATH, "//*[@id='root']/div/header/h1")
    actual = element.text
    expected = "Legal Resources"
    self.assertEqual(actual, expected)

  # Test that Physical model page loads as expected
  def test_PhysicalModelPage(self):
    self.driver.get(URL + "physical")
    element = self.driver.find_element(By.XPATH, "//*[@id='root']/div/header/h1")
    actual = element.text
    expected = "Physical Resources"
    self.assertEqual(actual, expected)

  # Test that Education model page's instance cards have clickable links
  # def test_EducationCards(self):
  #   self.driver.get(URL + "education")
  #   element = self driver.find_element(By.NAME, "visit_button")
      
  #   actual = element.text
  #   expected = "Visit"
  #   self.assertEqual(actual, expected)

  #   element.click()
  #   actual = self.driver.current_url
  #   expected = URL + "education/1"
  #   self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()

