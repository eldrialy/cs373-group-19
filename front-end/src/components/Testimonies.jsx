import React from 'react';
import { Container, Row, Col, Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import testimonials from '../data/testimonies';

const Testimonies = () => {
  return (
    <Container className="my-5" style={{ marginTop: '250px' }}>
      <Row className="justify-content-center">
        <Col > 
          <h2 className="text-center mb-4" style={{ fontFamily: "'Open Sans', sans-serif", fontWeight: 'bold', fontStyle: 'italic' }}>Survivor Stories</h2>
          <Carousel>
            {testimonials.map((testimonial) => (
              <Carousel.Item key={testimonial.id}>
                <div className="mb-5 text-center">
                  <h4 style={{ fontFamily: "'Roboto', sans-serif", fontWeight: '500', fontStyle: 'italic' }}>{testimonial.name}</h4>
                  <p style={{ fontFamily: "'Merriweather', serif", fontSize: '18px', fontStyle: 'italic' }}>
                    &ldquo;{testimonial.quote}&rdquo;
                  </p>
                  <p>
                    <Link to={testimonial.link} style={{ textDecoration: 'underline', color: '#007bff' }}>Read more</Link>
                  </p>
                </div>
              </Carousel.Item>
            ))}
          </Carousel>
        </Col>
      </Row>
    </Container>
  );
};

export default Testimonies;