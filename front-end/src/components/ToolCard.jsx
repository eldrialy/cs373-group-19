import React from "react";

const ToolCard = ({ tools }) => {
  const handleCardClick = (url) => {
    window.open(url, "_blank", "noopener,noreferrer");
  };

  return (
    <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
      {tools.map((tool, index) => (
        <div
          key={index}
          className="col"
          onClick={() => handleCardClick(tool.link)}
          style={{ cursor: "pointer" }}
        >
          <div className="card h-100 text-center shadow-sm">
            <div className="pt-3">
              <img 
                src={tool.logo}
                alt={`${tool.name} logo`}
                className="card-img-top img-fluid"
                style={{ maxHeight: "200px", objectFit: "contain" }}
              />
            </div>
            <div className="card-body d-flex flex-column">
              <h5 style={{ color: '#8732a8' }} className="card-title fw-bold">{tool.name}</h5>
              <p className="card-text flex-grow-1">{tool.use}</p>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default ToolCard;
