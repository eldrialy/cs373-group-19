import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

const SearchBar = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [isFocused, setIsFocused] = useState(false);
  let navigate = useNavigate();

  const handleSearch = (event) => {
    if (event.key === 'Enter') {
      navigate(`/search?query=${encodeURIComponent(searchQuery)}`);
    }
  };

  return (
    <div className="search-bar-container" style={{ position: 'relative', width: '320px', marginRight: '25px' }}>
      <input
        type="text"
        value={searchQuery}
        onChange={(e) => setSearchQuery(e.target.value)}
        onKeyPress={handleSearch}
        onFocus={() => setIsFocused(true)}
        onBlur={() => setIsFocused(false)}
        style={{
          padding: '10px',
          width: '300px',
          paddingLeft: '20px',
          border: '1px solid #ccc',
          borderRadius: '4px',
        }}
      />
      <label
        htmlFor="search"
        style={{
          position: 'absolute',
          left: '20px',
          top: isFocused || searchQuery ? '0px' : '10px',
          fontSize: isFocused || searchQuery ? '12px' : '16px',
          color: '#999',
          transition: 'all 0.2s',
          pointerEvents: 'none',
        }}
      >
        Search...
      </label>
    </div>
  );
};

export default SearchBar;