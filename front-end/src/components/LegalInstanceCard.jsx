import React from "react";
import Card from 'react-bootstrap/Card';
import { ListGroup } from "react-bootstrap";
import Button from 'react-bootstrap/Button';
import { highlightText } from './TextEdit';

const LegalInstanceCard = ({ resource, searchQuery }) => {
  const { id, name, borough, address, child_friendly, lgbtq_specialized,
    nonprofit, faith_based, website } = resource;

  return (
    <Card>
      <Card.Body>
        <Card.Title style={{ color: '#8732a8', height: '75px' }}>{highlightText(name, searchQuery)}</Card.Title>
        <ListGroup variant="flush" style={{ height: '350px' }}>
          <ListGroup.Item className="mb-1">Borough: {highlightText(borough, searchQuery)}</ListGroup.Item>
          <ListGroup.Item className="mb-1">Address: {highlightText(address, searchQuery)}</ListGroup.Item>
          <ListGroup.Item className="mb-1">Child-friendly: {child_friendly ? 'Yes' : 'No'}</ListGroup.Item>
          <ListGroup.Item className="mb-1">LGBTQ-specialized: {lgbtq_specialized ? 'Yes' : 'No'}</ListGroup.Item>
          <ListGroup.Item className="mb-1">Non-profit: {nonprofit ? 'Yes' : 'No'}</ListGroup.Item>
          <ListGroup.Item className="mb-1">Faith-based: {faith_based ? 'Yes' : 'No'}</ListGroup.Item>
          <ListGroup.Item className="mb-1">
            Website: {resource.website ? (
              <a href={resource.website} target="_blank" rel="noopener noreferrer">
                {highlightText(website, searchQuery)}
              </a>
            ) : (
              "Website not available"
            )}
          </ListGroup.Item>
        </ListGroup>
        <div className="text-center">
          <Button name="visit_button" variant="primary" style={{ backgroundColor: '#8732a8' }} href={`/legalinstance/${id}`} className="mt-3">Visit</Button>
        </div>
      </Card.Body>
    </Card>
  );
};

export default LegalInstanceCard;
