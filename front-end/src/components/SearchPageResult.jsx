import React, { useState, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import EducationInstanceCard from '../components/EducationInstanceCard';
import LegalInstanceCard from '../components/LegalInstanceCard';
import PhysicalInstanceCard from '../components/PhysicalInstanceCard';
import Pagination from "react-bootstrap/Pagination";
import {
  fetchEducationalResources,
  fetchLegalResources,
  fetchPhysicalResources
} from '../data/api';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

const SearchPageResult = () => {
  let [searchParams] = useSearchParams();
  const searchQuery = searchParams.get('query') ? searchParams.get('query').toLowerCase() : '';
  const [searchResults, setSearchResults] = useState({ educational: [], legal: [], physical: [] });
  const [isLoading, setIsLoading] = useState(true);
  const instancesOnPage = 6;
  const [currentPage, setCurrentPage] = useState(1);
  const [pageInfo, updatePages] = useState({
    pages: [], eduFirstItem: [],
    eduLastItem: [],
    legalFirstItem: [],
    legalLastItem: [],
    physFirstItem: [],
    physLastItem: []
  });
  const [activeTab, setActiveTab] = useState('educational');
  const setActiveTabAndResetPage = (key) => {
    setActiveTab(key);
    setCurrentPage(1);
  };

  useEffect(() => {
    const fetchAllResources = async () => {
      setIsLoading(true);
      try {
        const educationalResources = await fetchEducationalResources();
        const legalResources = await fetchLegalResources();
        const physicalResources = await fetchPhysicalResources();
        const queryTokens = searchQuery.split(' ').filter(token => token.trim() !== '');

        const calculateRelevance = (resource) => {
          const resourceText = `${resource.name} ${resource.address || ''} ${resource.borough || ''} ${resource.website || ''}`.toLowerCase();
          let score = 0;

          if (resourceText.includes(searchQuery)) {
            score += 100;
          }

          const matches = queryTokens.filter(token => resourceText.includes(token));
          score += matches.length * 10;
          return score;
        };

        const sortAndFilterResources = (resources) => {
          return resources.map(resource => ({
            ...resource,
            relevanceScore: calculateRelevance(resource)
          }))
            .filter(resource => resource.relevanceScore > 0)
            .sort((a, b) => b.relevanceScore - a.relevanceScore);
        };

        setSearchResults({
          educational: sortAndFilterResources(educationalResources),
          legal: sortAndFilterResources(legalResources),
          physical: sortAndFilterResources(physicalResources)
        });
      } finally {
        setIsLoading(false);
      }
    };

    if (searchQuery) {
      fetchAllResources();
    }
  }, [searchQuery]);

  useEffect(() => {
    const eduLength = searchResults.educational.length;
    const eduPages = Math.ceil(eduLength / instancesOnPage);
    let firstEduIndex = ((currentPage - 1) * instancesOnPage);
    const eduCount = searchResults.educational.slice(
      firstEduIndex,
      firstEduIndex + instancesOnPage).length;
    let lastEduIndex = firstEduIndex + eduCount;
    if (firstEduIndex > eduLength || eduCount == 0) {
      firstEduIndex = 0;
      lastEduIndex = 0;
    }


    const legalLength = searchResults.legal.length;
    const legalPages = Math.ceil(legalLength / instancesOnPage);
    let firstLegalIndex = ((currentPage - 1) * instancesOnPage);
    const legalCount = searchResults.legal.slice(
      firstLegalIndex,
      firstLegalIndex + instancesOnPage).length;
    let lastLegalIndex = firstLegalIndex + legalCount;
    if (firstLegalIndex > legalLength || legalCount == 0) {
      firstLegalIndex = 0;
      lastLegalIndex = 0;
    }

    const physicalLength = searchResults.physical.length;
    const physicalPages = Math.ceil(physicalLength / instancesOnPage);
    let firstPhysIndex = ((currentPage - 1) * instancesOnPage);
    const physCount = searchResults.physical.slice(
      firstPhysIndex,
      firstPhysIndex + instancesOnPage).length;
    let lastPhysIndex = firstPhysIndex + physCount;
    if (firstPhysIndex > physicalLength || physCount == 0) {
      firstPhysIndex = 0;
      lastPhysIndex = 0;
    }

    updatePages({
      pages: Math.max(eduPages, (legalPages, physicalPages)),
      eduFirstItem: firstEduIndex,
      eduLastItem: lastEduIndex,
      legalFirstItem: firstLegalIndex,
      legalLastItem: lastLegalIndex,
      physFirstItem: firstPhysIndex,
      physLastItem: lastPhysIndex
    });
  });

  const eduCurrentItems = searchResults.educational.slice(
    pageInfo.eduFirstItem,
    pageInfo.eduLastItem);
  const legalCurrentItems = searchResults.legal.slice(
    pageInfo.legalFirstItem,
    pageInfo.legalLastItem);
  const physicalCurrentItems = searchResults.physical.slice(
    pageInfo.physFirstItem,
    pageInfo.physLastItem);


  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
    window.scrollTo(0, 0);
  };

  const renderPaginationItems = () => {
    let paginationItems = [];
    const totalItems = activeTab === 'educational' ? searchResults.educational.length :
      activeTab === 'legal' ? searchResults.legal.length :
        activeTab === 'physical' ? searchResults.physical.length : 0;

    const totalPages = Math.ceil(totalItems / instancesOnPage);

    for (let number = 1; number <= totalPages; number++) {
      paginationItems.push(
        <Pagination.Item
          key={number}
          active={number === currentPage}
          onClick={() => handlePageChange(number)}
        >
          {number}
        </Pagination.Item>
      );
    }
    return paginationItems;
  };

  return (
    <div className="container my-5">

      <Container>
        {isLoading && (
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        )}
      </Container>
      <Tabs activeKey={activeTab} onSelect={(key) => setActiveTabAndResetPage(key)} className="justify-content-center">
        <Tab eventKey="educational" title="Educational Resources">
          <div style={{ marginTop: '25px', marginBottom: '20px' }}>
            <h2 style={{ marginBottom: '15px', textAlign: 'center' }}>Search Results for "{searchQuery}"...</h2>
            <h2 style={{ marginBottom: '15px', textAlign: 'center' }}> Results: {searchResults.educational.length}</h2>
            <h3 style={{ marginBottom: '15px' }}>Educational Resources</h3>
            <a>Showing {pageInfo.eduFirstItem + 1}-{pageInfo.eduLastItem} out of {searchResults.educational.length}</a>
            <Row xs={1} md={3} className="g-4">
              {eduCurrentItems.map(resource => (
                <Col key={resource.id}>
                  <EducationInstanceCard resource={resource} searchQuery={searchQuery} />
                </Col>
              ))}
            </Row>
          </div>
        </Tab>
        <Tab eventKey="legal" title="Legal Resources">
          <div style={{ marginTop: '25px', marginBottom: '20px' }}>
            <h2 style={{ marginBottom: '15px', textAlign: 'center' }}>Search Results for "{searchQuery}"...</h2>
            <h2 style={{ marginBottom: '15px', textAlign: 'center' }}> Results: {searchResults.legal.length}</h2>
            <h3 style={{ marginBottom: '15px' }}>Legal Resources</h3>
            <a>Showing {pageInfo.legalFirstItem + 1}-{pageInfo.legalLastItem} out of {searchResults.legal.length}</a>
            <Row xs={1} md={3} className="g-4">
              {legalCurrentItems.map(resource => (
                <Col key={resource.id}>
                  <LegalInstanceCard resource={resource} searchQuery={searchQuery} />
                </Col>
              ))}
            </Row>
          </div>
        </Tab>
        <Tab eventKey="physical" title="Physical Resources">
          <div style={{ marginTop: '25px', marginBottom: '20px' }}>
            <h2 style={{ marginBottom: '15px', textAlign: 'center' }}>Search Results for "{searchQuery}"...</h2>
            <h2 style={{ marginBottom: '15px', textAlign: 'center' }}> Results: {searchResults.physical.length}</h2>
            <h3 style={{ marginBottom: '15px' }}>Physical Resources</h3>
            <a>Showing {pageInfo.physFirstItem + 1}-{pageInfo.physLastItem} out of {searchResults.physical.length}</a>
            <Row xs={1} md={3} className="g-4">
              {physicalCurrentItems.map(resource => (
                <Col key={resource.id}>
                  <PhysicalInstanceCard resource={resource} searchQuery={searchQuery} />
                </Col>
              ))}
            </Row>
          </div>
        </Tab>
      </Tabs>
      <Container>
        <Pagination className="justify-content-center">
          {renderPaginationItems()}
        </Pagination>
      </Container>
    </div>
  );
};

export default SearchPageResult;