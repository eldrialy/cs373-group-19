import React from "react";

const DataSourceCard = ({ dataSources }) => {
  const handleCardClick = (url) => {
    window.open(url, "_blank", "noopener,noreferrer");
  };

  return (
    <div className="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
      {dataSources.map((source, index) => (
        <div
          key={index}
          className="col"
          onClick={() => handleCardClick(source.link)}
          style={{ cursor: "pointer" }}
        >
          <div className="card h-100 text-center shadow-sm">
            <div className="card-body d-flex align-items-center justify-content-center">
              <h5 style={{ color: '#8732a8' }} className="card-title mt-1 text-decoration-none fw-bold">
                {source.name}
              </h5>
            </div>
            <p className="card-text mt-1 mb-4">{source.description}</p>
          </div>
        </div>
      ))}
    </div>
  );
};

export default DataSourceCard;
