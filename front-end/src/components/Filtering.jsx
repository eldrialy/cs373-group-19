import React from 'react';
import { Form, Row, Col } from 'react-bootstrap';

const Filtering = ({ name_order, setNameOrder, borough, setBorough, faith_based, setFaithBased, nonprofit, setNonprofit, lgbtq_specialized, setLgbtqSpecialized, child_friendly, setChildFriendly }) => {
  return (
      <>
        <Form className="mb-3">
          <Row>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="name">
                <Form.Label>Sort by Name</Form.Label>
                <Form.Select value={name_order} onChange={(e) => setNameOrder(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="asc">Ascending</option>
                  <option value="desc">Descending</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="borough">
                <Form.Label>Borough</Form.Label>
                <Form.Select value={borough} onChange={(e) => setBorough(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="Manhattan">Manhattan</option>
                  <option value="Brooklyn">Brooklyn</option>
                  <option value="Queens">Queens</option>
                  <option value="Bronx">Bronx</option>
                  <option value="Staten Island">Staten Island</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="faith_based">
                <Form.Label>Faith-based</Form.Label>
                <Form.Select value={faith_based} onChange={(e) => setFaithBased(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="true">Yes</option>
                  <option value="false">No</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="nonprofit">
                <Form.Label>Non-profit</Form.Label>
                <Form.Select value={nonprofit} onChange={(e) => setNonprofit(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="true">Yes</option>
                  <option value="false">No</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="lgbtq_specialized">
                <Form.Label>LGBTQ-Specialized</Form.Label>
                <Form.Select value={lgbtq_specialized} onChange={(e) => setLgbtqSpecialized(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="true">Yes</option>
                  <option value="false">No</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="child_friendly">
                <Form.Label>Child-Friendly</Form.Label>
                <Form.Select value={child_friendly} onChange={(e) => setChildFriendly(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="true">Yes</option>
                  <option value="false">No</option>
                </Form.Select>
              </Form.Group>
            </Col>
          </Row>
        </Form>
      </>
    );
  };

export default Filtering;
