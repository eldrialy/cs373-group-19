import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { Navbar, Container, Nav } from 'react-bootstrap';
import logo from "./../assets/logo.png";
import SearchBar from './SearchBar';

const NavigationBar = () => {

  const navigate = useNavigate();

  const handleSearch = (query) => {
    navigate(`/search?query=${query}`);
  };

  return (
    <Navbar expand="lg" style={{ backgroundColor: "#e2dce6" }}>
      <Container>
        <Navbar.Brand href="/">
          <img
            src={logo}
            height="30"
            className="d-inline-block align-top"
            alt="Logo"
          />
          NYC Survivor Support
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/about">About</Nav.Link>
            <Nav.Link as={Link} to="/education">Education</Nav.Link>
            <Nav.Link as={Link} to="/legal">Legal Help</Nav.Link>
            <Nav.Link as={Link} to="/physical">Physical Help</Nav.Link>
            <Nav.Link as={Link} to="/visualizations">Visualizations</Nav.Link>
            <Nav.Link as={Link} to="/providervisualizations">Provider Visualizations</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
      <SearchBar onSearch={handleSearch} />
    </Navbar>
  );
};


export default NavigationBar;