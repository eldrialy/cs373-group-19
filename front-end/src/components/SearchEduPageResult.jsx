import React, { useState, useEffect } from 'react';
import { useSearchParams } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Spinner from 'react-bootstrap/Spinner';
import EducationInstanceCard from '../components/EducationInstanceCard';
import { fetchEducationalResources } from '../data/api';
import GenericSearch from '../components/GenericSearch';
import Pagination from "react-bootstrap/Pagination";
import { useNavigate } from "react-router-dom";

const SearchEduPageResult = () => {
  let [searchParams] = useSearchParams();
  const searchQuery = searchParams.get('query') ? searchParams.get('query').toLowerCase() : '';
  const [searchResults, setSearchResults] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const instancesOnPage = 12;

  const navigate = useNavigate();

  const handleSearch = (query) => {
    navigate(`/searchedu?query=${query}`);
  };

  useEffect(() => {
    const fetchEduResources = async () => {
      setIsLoading(true);
      try {
        const educationalResources = await fetchEducationalResources();
        const queryTokens = searchQuery.split(' ').filter(token => token.trim() !== '');

        const calculateRelevance = (resource) => {
          const resourceText = `${resource.name} ${resource.address || ''} ${resource.borough || ''} ${resource.website || ''}`.toLowerCase();
          let score = 0;
          if (resourceText.includes(searchQuery)) {
            score += 100;
          }
          const matches = queryTokens.filter(token => resourceText.includes(token));
          score += matches.length * 10;
          return score;
        };

        const sortedFilteredEducational = educationalResources
          .map(resource => ({
            ...resource,
            relevanceScore: calculateRelevance(resource)
          }))
          .filter(resource => resource.relevanceScore > 0)
          .sort((a, b) => b.relevanceScore - a.relevanceScore);

        setSearchResults(sortedFilteredEducational);
      } finally {
        setIsLoading(false);
      }
    };

    if (searchQuery) {
      fetchEduResources();
    }
  }, [searchQuery]);

  const numResults = searchResults.length;
  const pages = Math.ceil(numResults / instancesOnPage);
  let firstIndex = ((currentPage - 1) * instancesOnPage);
  const currentItems = searchResults.slice(
    firstIndex,
    firstIndex + instancesOnPage);
  let lastIndex = firstIndex + currentItems.length;
  if (firstIndex > numResults || currentItems == 0) {
    firstIndex = 0;
    lastIndex = 0;
  }

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
    window.scrollTo(0, 0);
  };

  const renderPaginationItems = () => {
    let paginationItems = [];
    for (let number = 1; number <= pages; number++) {
      paginationItems.push(
        <Pagination.Item
          key={number}
          active={number === currentPage}
          onClick={() => handlePageChange(number)}
        >
          {number}
        </Pagination.Item>
      );
    }
    return paginationItems;
  };

  return (
    <div className="container my-5">
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <GenericSearch basePath="searchedu" placeholder="Search Education" onSearch={handleSearch} />
      </div>
      <h2 style={{ marginBottom: '15px' }}>Search Results for "{searchQuery}"...</h2>
      <h4 style={{ marginBottom: '15px' }}> Showing {firstIndex + 1}-{lastIndex} of {searchResults.length} results </h4>
      <Container>
        <Pagination className="justify-content-center">
          {renderPaginationItems()}
        </Pagination>
        {isLoading ? (
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        ) : (
          <div style={{ marginTop: '25px', marginBottom: '20px' }}>
            <h3 style={{ marginBottom: '15px' }}>Educational Resources</h3>
            <Row xs={1} md={3} className="g-4">
              {currentItems.map(resource => (
                <Col key={resource.id}>
                  <EducationInstanceCard resource={resource} searchQuery={searchQuery} />
                </Col>
              ))}
            </Row>
          </div>
        )}
        <Pagination className="justify-content-center">
          {renderPaginationItems()}
        </Pagination>
      </Container>
    </div>
  );
};

export default SearchEduPageResult;