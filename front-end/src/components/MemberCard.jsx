import React from "react";

const MemberCard = ({ member, activity }) => {
  return (
    <div className="col-md-4 mb-4">
      <div className="card h-100 shadow-sm">
        <img
          src={member.image}
          alt={member.name}
          className="card-img-top"
          style={{ height: "400px", objectFit: "cover" }}
        />
        <div className="card-body d-flex flex-column">
          <h5 className="card-title mb-3">{member.name}</h5>
          <p className="card-text mb-2">{member.bio}</p>
          <p className="card-text mb-1">
            <strong>Role:</strong> {member.role}
          </p>
          <p className="card-text mb-1">
            <strong>Commits:</strong> {activity.commits}
          </p>
          <p className="card-text mb-1">
            <strong>Issues:</strong> {activity.issues}
          </p>
          <p className="card-text">
            <strong>Unit Tests:</strong> {member.tests}
          </p>
        </div>
      </div>
    </div>
  );
};

export default MemberCard;
