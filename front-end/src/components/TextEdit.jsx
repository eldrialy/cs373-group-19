export const highlightText = (text, query) => {
  const safeText = text ?? '';
  
  if (!query) return safeText;
  const escapedQuery = query.replace(/[-\\^$*+?.()|[\]{}]/g, '\\$&');
  const pattern = escapedQuery.split(' ').filter(word => word.trim() !== '').join('|');
  const regex = new RegExp(`(${pattern})`, 'gi');
  const parts = safeText.split(regex);

  return (
    <>
      {parts.map((part, index) =>
        regex.test(part) ? <mark key={index} style={{ backgroundColor: 'yellow', color: 'black' }}>{part}</mark> : part
      )}
    </>
  );
};