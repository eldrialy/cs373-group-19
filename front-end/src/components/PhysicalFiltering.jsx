import React from 'react';
import { Form, Row, Col } from 'react-bootstrap';

const PhysicalFiltering = ({ name_order, setNameOrder, borough, setBorough, language_svcs, setLanguage, wheelchair_access, setWheelchairAccess, lgbtq_specialized, setLgbtqSpecialized, child_friendly, setChildFriendly }) => {
  return (
      <>
        <Form className="mb-3">
          <Row>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="name">
                <Form.Label>Sort by Name</Form.Label>
                <Form.Select value={name_order} onChange={(e) => setNameOrder(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="asc">Ascending</option>
                  <option value="desc">Descending</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="borough">
                <Form.Label>Borough</Form.Label>
                <Form.Select value={borough} onChange={(e) => setBorough(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="Manhattan">Manhattan</option>
                  <option value="Brooklyn">Brooklyn</option>
                  <option value="Queens">Queens</option>
                  <option value="Bronx">Bronx</option>
                  <option value="Staten Island">Staten Island</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="language_svcs">
                <Form.Label>Language Services</Form.Label>
                <Form.Select value={language_svcs} onChange={(e) => setLanguage(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="English">English</option>
                  <option value="Spanish">Spanish</option>
                  <option value="Haitian Creole">Haitian Creole</option>
                  <option value="American Sign Language (ASL)">American Sign Language (ASL)</option>
                  <option value="French">French</option>
                  <option value="Albanian">Albanian</option>
                  <option value="Bengali">Bengali</option>
                  <option value="Chinese">Chinese</option>
                  <option value="Hindi">Hindi</option>
                  <option value="Korean">Korean</option>
                  <option value="Polish">Polish</option>
                  <option value="Russian">Russian</option>
                  <option value="Urdu">Urdu</option>
                  <option value="Arabic">Arabic</option>
                  <option value="Other">Other</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="wheelchair_access">
                <Form.Label>Wheelchair Access</Form.Label>
                <Form.Select value={wheelchair_access} onChange={(e) => setWheelchairAccess(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="true">Yes</option>
                  <option value="false">No</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="lgbtq_specialized">
                <Form.Label>LGBTQ-Specialized</Form.Label>
                <Form.Select value={lgbtq_specialized} onChange={(e) => setLgbtqSpecialized(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="true">Yes</option>
                  <option value="false">No</option>
                </Form.Select>
              </Form.Group>
            </Col>
            <Col className="mb-3" sm={4}>
              <Form.Group controlId="child_friendly">
                <Form.Label>Child-Friendly</Form.Label>
                <Form.Select value={child_friendly} onChange={(e) => setChildFriendly(e.target.value)}>
                  <option value="">Select...</option>
                  <option value="true">Yes</option>
                  <option value="false">No</option>
                </Form.Select>
              </Form.Group>
            </Col>
          </Row>
        </Form>
      </>
    );
  };

export default PhysicalFiltering;