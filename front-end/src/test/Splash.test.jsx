import { render, screen } from '@testing-library/react';
import "@testing-library/jest-dom";
import React from "react";
import { MemoryRouter } from 'react-router-dom';
import Splash from '../pages/splash.jsx';
import Testimonies from '../components/Testimonies.jsx';
import NavigationBar from '../components/NavigationBar.jsx';


test('Splash page renders corrently', async() => {
  render(<MemoryRouter>
    <Splash />
  </MemoryRouter>);
  expect(screen.getByText("NYC Survivor Support")).toBeInTheDocument();
  expect(screen.getByText("Education")).toBeInTheDocument();
  expect(screen.getByText("Get information on how navigate a domestic violence situation, or learn ways to support victims of domestic violence.")).toBeInTheDocument();
  expect(screen.getByText("Legal Resources")).toBeInTheDocument();
  expect(screen.getByText("Find legal assistance for individuals or families experiencing domestic violence situations.")).toBeInTheDocument();
  expect(screen.getByText("Physical Resources")).toBeInTheDocument();
  expect(screen.getByText("Locate physical resources for victims of domestic violence, including shelters, donation centers, and more.")).toBeInTheDocument();
  expect(screen.getByText("Survivor Stories")).toBeInTheDocument();
});

// need to add actual testing for functionality not just display
test('NavBar is rendered correctly and works', () => {
  render(<MemoryRouter>
          <NavigationBar />
        </MemoryRouter>);
  expect(screen.getByText("NYC Survivor Support")).toBeInTheDocument();
  expect(screen.getByText("Home")).toBeInTheDocument();
  expect(screen.getByText("About")).toBeInTheDocument();
  expect(screen.getByText("Get Educated")).toBeInTheDocument();
  expect(screen.getByText("Legal Assistance")).toBeInTheDocument();
  expect(screen.getByText("Find Physical Resources")).toBeInTheDocument();
});
