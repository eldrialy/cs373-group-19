import "@testing-library/jest-dom";
import { render, screen } from '@testing-library/react';
import { expect } from "@jest/globals";
import { MemoryRouter } from "react-router-dom";
import React from "react";
import EducationInstance from "../pages/educationinstance.jsx";
import LegalInstance from "../pages/legalinstance.jsx";
import PhysicalInstance from "../pages/physicalinstance.jsx";
import { fetchEducationalResources } from "../data/api"; 
 
describe('Instance Page tests', () => {
  test('Education Instance page renders', async() => {
    render (<MemoryRouter>
              <EducationInstance
                id={80}
                />
          </MemoryRouter>);
    //expect(await screen.findByText("GEMS Girls Educational and Mentoring Services"), {}).toBeInTheDocument();
   // expect(await screen.findByText("Loading")).toBeInTheDocument();
    screen.debug();
  });

  // test('Legal Instance page renders', async() => {
  //   render (<MemoryRouter>
  //             <LegalInstance
  //               id={1}
  //               />
  //         </MemoryRouter>)
  //   // expect(await screen.findByText(" "), {}, {timeout: 1000}).toBeInTheDocument();
  //   screen.debug();
  // });

  // test('Physical Instance page renders', async() => {
  //   render (<MemoryRouter>
  //             <PhysicalInstance
  //               id={1}               
  //               />
  //         </MemoryRouter>)
  //   // expect(await screen.findByText(" "), {}, {timeout: 1000}).toBeInTheDocument();
  //   screen.debug();
  // });
});
