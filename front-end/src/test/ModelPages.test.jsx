import "@testing-library/jest-dom";
import { render, screen, waitFor, act } from '@testing-library/react';
import { expect } from "@jest/globals";
import { MemoryRouter } from "react-router-dom";
import axios from 'axios';
import React from "react";
import Education from "../pages/education.jsx";
import Legal from "../pages/legal.jsx";
import Physical from "../pages/physical.jsx";
import { fetchEducationalResources } from "../data/api"; 
import { fetchLegalResources } from "../data/api"; 
import { fetchPhysicalResources } from "../data/api"; 

jest.mock('axios');

describe('Model Page tests', () => {
  test('Education model page renders correctly', async() => {
    act(() => {
      axios.get.mockResolvedValueOnce({ data: 'Education Resources' });
      ({ getByText, unmount } = render (
      <MemoryRouter>
        <Education />
      </MemoryRouter>));
    });
    await waitFor(() => getByText('Education Resources'));
    unmount();
  });

  test('Legal model page renders correctly', async() => {
    act(() => {
      axios.get.mockResolvedValueOnce({ data: 'Legal Resources' });
      ({ getByText, unmount } = render (
      <MemoryRouter>
        <Legal />
      </MemoryRouter>));
    });
    await waitFor(() => getByText('Legal Resources'));
    unmount();
  });

  test('Physical model page renders correctly', async() => {
    act(() => {
      axios.get.mockResolvedValueOnce({ data: 'Physical Resources' });
      ({ getByText, unmount } = render (
      <MemoryRouter>
        <Physical />
      </MemoryRouter>));
    });
    await waitFor(() => getByText('Physical Resources'));
    unmount();
  });
});