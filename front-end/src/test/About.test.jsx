import "@testing-library/jest-dom";
import { render, screen, userEvent, fireEvent } from '@testing-library/react';
import { expect } from "@jest/globals";
import React from "react";
import DataSourceCard from '../components/DataSourceCard.jsx';
import ToolCard from '../components/ToolCard.jsx';
import About from '../pages/about.jsx';
import { MemoryRouter } from "react-router-dom";

describe('About page tests', () => {
  test('About page renders', () => {
    render (<MemoryRouter>
              <About />
            </MemoryRouter>);
    expect(screen.getByText("About Us")).toBeInTheDocument();
    expect(screen.getByText("Our Team")).toBeInTheDocument();
    expect(screen.getByText("Total Statistics")).toBeInTheDocument();
    expect(screen.getByText("Data Sources")).toBeInTheDocument();
    expect(screen.getByText("Interesting Results of Integrating Disparate Data")).toBeInTheDocument();
    expect(screen.getByText("Tools Used")).toBeInTheDocument();
  });

  test('Team member info shows up', () => {
    render (<MemoryRouter>
      <About />
    </MemoryRouter>);
    expect(screen.getByText("Jamie Babbe")).toBeInTheDocument();
    expect(screen.getByText("Alyssa Eldridge")).toBeInTheDocument();
    expect(screen.getByText("Ava Machado")).toBeInTheDocument();
    expect(screen.getByText("PJ Shenoy")).toBeInTheDocument();
    expect(screen.getByText("Francis Yeh")).toBeInTheDocument();
    expect(screen.getByText("Total Commits:")).toBeInTheDocument();
  });

  // coverage isn't 100% due to onCardClick links not being reached
  test('ToolCard is rendered correctly', () => {
    const tools = [{
      name: "AWS Amplify",
      link: "https://aws.amazon.com/amplify/",
      use: "Used for hosting the website",
      logo: "./../src/assets/amplify.svg",
    },];
    render(<ToolCard  
      tools={tools} />);
    expect(screen.getByText("AWS Amplify")).toBeInTheDocument();
  });

  test('DataSourceCard is rendered correctly', () => {
    const datasources = [{
      name: "NYC Women's Resource Network Database",
      link: "https://data.cityofnewyork.us/Social-Services/NYC-Women-s-Resource-Network-Database/pqg4-dm6b/data",
      description: "Data scraped using REST API calls",
    },];
    // const handleCardClickS = jest.fn();
    // const { getByTestId } = render(
    //   <DataSourceCard dataSources={datasources}
    //                   onClick={handleCardClickS} />
    // );
    // //userEvent.click(screen.getByTestId('product'));
    // fireEvent.click(getByTestId({datasources}));
    // expect(handleCardClickS).toBeCalledTimes(1);
    // expect(handleCardClickS).toHaveBeenCalled();
    render(<DataSourceCard  
      dataSources={datasources} />);
    expect(screen.getByText("NYC Women's Resource Network Database")).toBeInTheDocument();
  });
});