import React from 'react';
import Badge from 'react-bootstrap/Badge';
import ListGroup from 'react-bootstrap/ListGroup';
import DVA from "./../assets/DVA.png";
import holdhands from "./../assets/holdhands.png";
import resource from "./../assets/resource.png";
import support from "./../assets/support.png";
import Carousel from 'react-bootstrap/Carousel';
import hug from "./../assets/hug.png";
import love from "./../assets/loveshouldn'thurt.png";
import stop from "./../assets/stopviolence.png";
import Testimonies from '../components/Testimonies';
import './MainTitle.css';
import './Splash.css';
import { Link } from 'react-router-dom';

const Splash = () => {
    return (
        <div className="home-page">
            <header className="custom-header text-center my-5 py-4">
                <h1 className="mb-4 display-6" style={{ fontFamily: "'Poppins', sans-serif", fontWeight: 'bold', fontStyle: 'italic', color: 'white', textShadow: '2px 2px 4px rgba(0, 0, 0, 0.2)' }}>NYC Survivor Support</h1>
            </header>
            <div className="container d-flex flex-column">
                <div className="row">
                    <div className="col-md-6">
                        <p className="intro-paragraph">NYC Survivor Support aims to bring awareness to survivors of domestic violence.
                            Not only is this an educational site, but it is also useful for resources for both victims and those who wish to offer support. The categories
                            below will provide information about legal resources for victims, and also physical resources for both victims and supporters alike.</p>
                        <div className="d-flex justify-content-between">
                            <ListGroup as="ol" className="text-center">
                                <ListGroup.Item as="li" style={{ height: '225px' }} className="list-group-custom-item">
                                    <Link style={{ color: '#8732a8' }} to="/education">
                                        <div className="fw-bold">Education</div>
                                    </Link>
                                    <p className="text-center">Get information on how navigate a domestic violence situation, or learn ways to support victims of domestic violence.</p>
                                    <Badge bg="primary" pill></Badge>
                                </ListGroup.Item>
                            </ListGroup>
                            <ListGroup as="ol" className="text-center">
                                <ListGroup.Item as="li" style={{ height: '225px' }} className="list-group-custom-item">
                                    <Link style={{ color: '#8732a8' }} to="/legal">
                                        <div className="fw-bold">Legal Resources</div>
                                    </Link>
                                    <p className="text-center">Find legal assistance for individuals or families experiencing domestic violence situations.</p>
                                    <Badge bg="primary" pill></Badge>
                                </ListGroup.Item>
                            </ListGroup>
                            <ListGroup as="ol" className="text-center">
                                <ListGroup.Item as="li" style={{ height: '225px' }} className="list-group-custom-item">
                                    <Link style={{ color: '#8732a8' }} to="/physical">
                                        <div className="fw-bold">Physical Resources</div>
                                    </Link>
                                    <p className="text-center">Locate physical resources for victims of domestic violence, including shelters, donation centers, and more.</p>
                                    <Badge bg="primary" pill></Badge>
                                </ListGroup.Item>
                            </ListGroup>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="image-container">
                            <img src={hug} alt="Stop Domestic Violence" style={{ width: '400px', height: '500px', marginLeft: '125px' }} />
                        </div>
                    </div>
                </div>
                <div className="row mt-5 justify-content-center">
                    <div className="col-md-6 text-center" style={{marginTop: '57px'}}>
                        <h3>NYCSurvivorSupport Domestic Violence Informational</h3>
                        <p>Check out our YouTube channel for more information and resources.</p>
                        <a href="https://youtu.be/VNPBFa_2OHU" target="_blank" rel="noopener noreferrer">Watch Now</a>
                    </div>
                    <div className="col-md-6">
                        <Testimonies />
                    </div>
                </div>
                <div className="row mt-5">
                    <div className="col-md-12" style={{ backgroundColor: '#e2dce6', padding: '20px' }}>
                        <Carousel className="w-100">
                            <Carousel.Item>
                                <img className="d-block mx-auto" src={DVA} alt="First slide" style={{ width: '70%', height: 'auto', maxHeight: '500px' }} />
                                <Carousel.Caption>
                                    <p>Official Ribbon for Domestic Violence Awareness</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img className="d-block mx-auto" src={support} alt="Second slide" style={{ width: '70%', height: 'auto', maxHeight: '500px' }} />
                                <Carousel.Caption>
                                    <p>Find Support For You or Someone You Know</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img className="d-block mx-auto" src={resource} alt="Third slide" style={{ width: '70%', height: 'auto', maxHeight: '500px' }} />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img className="d-block mx-auto" src={holdhands} alt="Fourth slide" style={{ width: '70%', height: 'auto', maxHeight: '500px' }} />
                                <Carousel.Caption>
                                    <p>Support Domestic Violence Awareness</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img className="d-block mx-auto" src={love} alt="Fifth slide" style={{ width: '70%', height: 'auto', maxHeight: '500px' }} />
                            </Carousel.Item>
                            <Carousel.Item>
                                <img className="d-block mx-auto" src={stop} alt="Sixth slide" style={{ width: '70%', height: 'auto', maxHeight: '500px' }} />
                            </Carousel.Item>
                        </Carousel>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Splash;
