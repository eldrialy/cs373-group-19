import React from "react";
import { team, data_sources, tools } from "./../data/about";
import MemberCard from "./../components/MemberCard";
import DataSourceCard from "./../components/DataSourceCard";
import ToolCard from "../components/ToolCard";
import TeamActivity from "../hooks/TeamActivity";

const About = () => {
  const { memberActivity, totalStats } = TeamActivity(team);

  return (
    <>
      <header className="container my-5">
        <h1 name="site_info" className="text-center mb-4 display-6">
          About Us
        </h1>
        <p className="text-center lead">
          Our website serves to support and empower victims of domestic violence
          living in New York City, NY by providing educational, legal, and
          physical resources. NYC Survivor Support primarily aims to assist our
          users by guiding survivors through the complexities of the legal
          system, offering immediate physical support through shelters and
          counseling services, and fostering awareness and prevention through
          educational initiatives and statistics. We are dedicated to creating a
          site that not only promotes a safe healing process for survivors but
          also equips others with the skills and knowledge needed to offer
          effective support to victims in their life.
        </p>
      </header>
      <section className="container">
        <h2 name="team_info" className="text-center mb-4 display-6">Our Team</h2>
        <div className="row justify-content-center">
          {team.map((member) => {
            const activity = memberActivity[member.name] || {
              commits: 0,
              issues: 0,
            };
            return (
              <MemberCard
                key={member.name}
                member={member}
                activity={activity}
              />
            );
          })}
        </div>
      </section>
      <section className="container my-4">
        <h2 className="text-center mb-4 display-6">Total Statistics</h2>
        <div className="text-center lead">
          <p>
            <strong className="fw-bold">Total Commits: </strong>
            {totalStats.commits}
          </p>
          <p>
            <strong className="fw-bold">Total Issues: </strong>
            {totalStats.issues}
          </p>
          <p>
            <strong className="fw-bold">Total Unit Tests: </strong>
            {totalStats.tests}
          </p>
        </div>
      </section>
      <section className="container my-5">
        <h2 className="text-center mb-4 display-6">Data Sources</h2>
        <DataSourceCard dataSources={data_sources} />
      </section>
      <section className="container my-5">
        <h2 className="text-center mb-4 display-6">
          Interesting Results of Integrating Disparate Data
        </h2>
        <p className="text-center lead">
          It was interesting various data sources could be related to the
          same resource yet provide different information. This was useful in
          cases where the varying data allowed us to provide more comprehensive
          information for our users. However, this caused difficulty ensuring
          accuracy if data sources had conflicting information.
        </p>
      </section>
      <section className="container my-5">
        <h2 className="text-center mb-4 display-6">Tools Used</h2>
        <ToolCard tools={tools} />
      </section>
      <section className="container my-5">
        <div className="text-center">
        <a href="https://gitlab.com/eldrialy/cs373-group-19" className="btn btn-outline-primary btn-lg me-5 fs-2" target="_blank" rel="noopener noreferrer">
          GitLab Repository
        </a>
        <a href="https://documenter.getpostman.com/view/32924129/2sA3Bn5CNu" className="btn btn-outline-primary btn-lg fs-2" target="_blank" rel="noopener noreferrer">
          Postman API
        </a>
      </div>
    </section>
    </>
  );
};

export default About;
