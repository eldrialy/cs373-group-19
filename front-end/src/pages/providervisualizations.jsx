import CountyChart from '../data/developer_visualizations/CountyChart';
import STDMapChart from '../data/developer_visualizations/stdsMap';
import GCasesChart from '../data/developer_visualizations/gCasesChart';

const ProviderVisualizations = () => {
  return (
    <>
      <div className="container my-5 mx-auto row">
        <h1 className="text-center mb-4 display-6">Provider Visualizations</h1>
        <div className="container my-5 mx-auto row">
            <div className="container my-5 mx-auto row">
            <h2 className="text-center mb-4 display-6">STD prevalency over the years in CA</h2>
            <div className="col justify-content-md-center center-block" align="center">
                <STDMapChart/>
            </div>
            </div>

            <div className="container my-5 mx-auto row">
            <h2 className="text-center mb-4 display-6">Gonnorhea cases by county in CA in 2021</h2>
            <div className="col justify-content-md-center center-block" align="center">
                <GCasesChart/>
            </div>
            </div>

            <div className="container my-5 mx-auto row">
            <h2 className="text-center mb-4 display-6">Syphilis Cases by County in CA in 2021</h2>
            <div className="col justify-content-md-center center-block" align="center">
                <CountyChart/>
            </div>
            </div>
            
          </div>
        <div className="container my-5 mx-auto row">
          <h2 className="text-center mb-4 display-6">Critique</h2>
          <ul>
            <li>
              What did they do well?
              <ul>
                <li>
                  They did a great job at ensuring their website was responsive.
                  It was also very colorful which made it pleasing for viewers.
                </li>
              </ul>
            </li>
            <li>
              How effective was their RESTful API?
              <ul>
                <li>
                  Their RESTful API was well built. Their documentation was easy
                  to follow which allowed us to create our visualizations
                  efficiently.
                </li>
              </ul>
            </li>
            <li>
              How well did they implement your user stories?
              <ul>
                <li>
                  They did a great job at implementing our user stories. They
                  efficiently and accurately made each adjustment we requested.
                </li>
              </ul>
            </li>
            <li>
              What did we learn from their website?
              <ul>
                <li>
                  We learned about the prevalence of various STDs within
                  California and where patients could find adequate treatment.
                </li>
              </ul>
            </li>
            <li>
              What can they do better?
              <ul>
                <li>
                  They could have better attention to detail. We noticed a
                  couple areas of the website that are inconsistent or could be
                  slightly more accurate. Overall though, they did a great job.
                </li>
              </ul>
            </li>
            <li>
              What puzzles us about their website?
              <ul>
                <li>
                  The prevalence of STDs from 2001-2021 is a bit puzzling. There
                  are over 3500 search results which is difficult to sift
                  through as a user just looking to be educated. It might have
                  been easier to understand the use case if certain data was
                  combined or the range of years was potentially shortened.
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default ProviderVisualizations;
