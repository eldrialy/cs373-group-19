import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Container from 'react-bootstrap/Container';
import { Row, Col } from 'react-bootstrap';
import Spinner from "react-bootstrap/Spinner";
import styles from './ModelStyle.module.css';
import { fetchEducationalResourceById } from '../data/api';
import PhysicalInstanceCard from "../components/PhysicalInstanceCard";
import LegalInstanceCard from '../components/LegalInstanceCard';

const EducationInstance = () => {
  const { id } = useParams();
  const [resource, setResource] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [physical, setPhysical] = useState(null);
  const [legal, setLegal] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const currentResource = await fetchEducationalResourceById(id);
        if (!currentResource) {
          throw new Error('Resource not found');
        }
        const physical = currentResource.associations.physical;
        const legal = currentResource.associations.legal;
        setPhysical(physical);
        setLegal(legal);
        setResource(currentResource.resource);
        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
      }
    };

    fetchData();
  }, [id]);

  if (loading) {
    return (
      <div className="text-center p-3">
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </div>
    );
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (!resource) {
    return <div>Resource not found</div>;
  }

  return (
    <>
      <Container>
        <Card border="primary" className="mx-auto">
          <Card.Img className="mx-auto d-block" style={{ height: 'auto', maxWidth: '50%' }} variant="top" src={resource.img_url ? resource.img_url : require("./../assets/educationimg.png")} />
          <Card.Body>
            <Card.Title>{resource.name}</Card.Title>
            <Card.Text>
              {resource.description}
            </Card.Text>
            <ListGroup variant="flush">
              <ListGroup.Item>Borough: {resource.borough}</ListGroup.Item>
              <ListGroup.Item>Address: {resource.address}</ListGroup.Item>
              <ListGroup.Item>Children-friendly: {resource.children_friendly ? 'Yes' : 'No'}</ListGroup.Item>
              <ListGroup.Item>LGBTQ-specialized: {resource.lgbtq_friendly ? 'Yes' : 'No'}</ListGroup.Item>
              <ListGroup.Item>Non-profit: {resource.non_profit ? 'Yes' : 'No'}</ListGroup.Item>
              <ListGroup.Item>Faith-based: {resource.faith_based ? 'Yes' : 'No'}</ListGroup.Item>
            </ListGroup>
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
              {resource.website && <Card.Link href={resource.website}>Click Here to Visit this Site</Card.Link>}
            </div>
            {resource.map_url && (
              <iframe
                width="450"
                height="250"
                frameBorder="0"
                style={{ border: "0" }}
                referrerPolicy="no-referrer-when-downgrade"
                src={resource.map_url}
                title={`${resource.name} map`}
                allowFullScreen
              ></iframe>
            )}
          </Card.Body>
        </Card>
      </Container>

      <Container>
        <header className={styles.titleContainer}>
          <h1 className={`text-center mb-4 display-6 ${styles.titleText}`}>Related Resources</h1>
        </header>
        <div>
          <Row>
            <Col sm={6}>
              <h4>Nearby physical resource:</h4>
              <PhysicalInstanceCard resource={physical} />
            </Col>
            <Col sm={6}>
              <h4>Nearby legal resource:</h4>
              <LegalInstanceCard resource={legal} />
            </Col>
          </Row>
        </div>
      </Container>
    </>
  );
};

export default EducationInstance;
