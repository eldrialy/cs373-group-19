import BoroughChart from '../data/boroughChart';
import LanguageChart from '../data/languageChart';
import HeatMapChart from '../data/resourceAvailability';

function Visualizations() {
  return (
    <>
      <div className="container my-5 mx-auto row">
        <h1 className="text-center mb-4 display-6">Visualizations</h1>
        <div className="container my-5 mx-auto row">
            <div className="container my-5 mx-auto row">
            <h2 className="text-center mb-4 display-6">Resources in Each Borough</h2>
            <div className="col justify-content-md-center center-block" align="center">
                <BoroughChart />
            </div>
            </div>
            <div className="container my-5 mx-auto row">
            <h2 className="text-center mb-4 display-6">Physical Resource Language Services</h2>
            <div className="col justify-content-md-center center-block" align="center">
                <LanguageChart />
            </div>
            </div>
            <div className="container my-5 mx-auto row">
            <h2 className="text-center mb-4 display-6">Resource Distribution</h2>
            <div className="col justify-content-md-center center-block" align="center">
                <HeatMapChart />
            </div>
            </div>
        </div>
        <div className="container my-5 mx-auto row">
          <h2 className="text-center mb-4 display-6">Self Critique</h2>
          <ul>
            <li>
              What did we do well?
              <ul>
                <li>
                  We did a great job at overcoming a difficult situation
                  regarding losing a team member and all of our hosting being
                  taken down. We were able to work together well in the face of
                  this challenge as well as other ones that popped up throughout
                  the course of this class.
                </li>
              </ul>
            </li>
            <li>
              What did we learn?
              <ul>
                <li>
                  We learned how to create a responsive full stack website. None
                  of us were super familiar with front-end development in
                  particular, so we learned a lot about using React and
                  JavaScript.
                </li>
              </ul>
            </li>
            <li>
              What did we teach each other?
              <ul>
                <li>
                  We were able to help each other out whenever someone ran into
                  a tricky bug. We all tried our best to learn each aspect of
                  the website in order to have a better understanding of what
                  was going on to contribute if anyone was stuck.
                </li>
              </ul>
            </li>
            <li>
              What can we do better?
              <ul>
                <li>
                  At times we finished things relatively last minute which led
                  to not discovering issues until the day the project was due.
                  This made for a few stressful situations, but overall we did
                  well tackling them as a team.
                </li>
              </ul>
            </li>
            <li>
              What effect did the peer reviews have?
              <ul>
                <li>
                  The peer reviews allowed us to reflect on how we can be a
                  better team member and helped us stay accountable.
                </li>
              </ul>
            </li>
            <li>
              What puzzles us?
              <ul>
                <li>
                  We are still a little puzzled regarding hosting because the
                  team member who primarily took care of that left without
                  warning. This caused a lot of confusion for us and stress
                  involving if we needed to get everything set up from scratch
                  last minute without prior knowledge of how to do it.
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}

export default Visualizations;