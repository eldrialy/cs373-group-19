import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";
import PhysicalInstanceCard from "../components/PhysicalInstanceCard";
import { fetchPhysicalResources } from "../data/api";
import styles from "./ModelStyle.module.css";
import Pagination from "react-bootstrap/Pagination";
import GenericSearch from '../components/GenericSearch';
import { useNavigate } from "react-router-dom";
import PhysicalFiltering from "../components/PhysicalFiltering";

function Physical() {
  const [physicalResources, setPhysicalResources] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
  const instancesOnPage = 12;
  const [error, setError] = useState(null);
  const [name_order, setNameOrder] = useState('');
  const [borough, setBorough] = useState('');
  const [language_svcs, setLanguage] = useState('');
  const [lgbtq_specialized, setLgbtqSpecialized] = useState('');
  const [child_friendly, setChildFriendly] = useState('');
  const [wheelchair_access, setWheelchairAccess] = useState('');

  const navigate = useNavigate();

  const handleSearch = (query) => {
    navigate(`/searchphys?query=${query}`);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const resources = await fetchPhysicalResources({
          name_order,
          borough,
          language_svcs,
          lgbtq_specialized,
          child_friendly,
          wheelchair_access,
        });
        setPhysicalResources(resources);
      } catch (error) {
        setError(error.message);
        console.error("Failed to fetch physical resources:", error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, [name_order, borough, language_svcs, lgbtq_specialized, child_friendly, wheelchair_access]);

  if (error) {
    return <div>Error: {error}</div>;
  }

  const itemLength = physicalResources.length;
  const pageLength = Math.ceil(itemLength / instancesOnPage);
  const indexOfLastItem = currentPage * instancesOnPage;
  const indexOfFirstItem = indexOfLastItem - instancesOnPage;
  const currentItems = physicalResources.slice(
    indexOfFirstItem,
    indexOfLastItem
  );

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
    window.scrollTo(0, 0);
  };

  const renderPaginationItems = () => {
    let paginationItems = [];
    for (let number = 1; number <= pageLength; number++) {
      paginationItems.push(
        <Pagination.Item
          key={number}
          active={number === currentPage}
          onClick={() => handlePageChange(number)}
        >
          {number}
        </Pagination.Item>
      );
    }
    return paginationItems;
  };

  return (
    <div className="container my-5">
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <GenericSearch basePath="searchphys" placeholder="Search Physical" onSearch={handleSearch} />
      </div>
      <header className={styles.titleContainer}>
        <h1 className={`text-center mb-4 display-6 ${styles.titleText}`}>
          Physical Resources
        </h1>
        <p className="text-center text-white">
          Showing {indexOfFirstItem + 1}-{indexOfFirstItem + currentItems.length} of {itemLength} total resources
        </p>
      </header>
      <PhysicalFiltering
        name_order={name_order}
        setNameOrder={setNameOrder}
        borough={borough}
        setBorough={setBorough}
        language_svcs={language_svcs}
        setLanguage={setLanguage}
        wheelchair_access={wheelchair_access}
        setWheelchairAccess={setWheelchairAccess}
        lgbtq_specialized={lgbtq_specialized}
        setLgbtqSpecialized={setLgbtqSpecialized}
        child_friendly={child_friendly}
        setChildFriendly={setChildFriendly}
      />
      <Pagination className="justify-content-center">
        {renderPaginationItems()}
      </Pagination>
      <Container>
        {isLoading ? (
          <div className="text-center p-3">
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          </div>
        ) : (
          <Row xs={1} md={3} className="g-4">
            {currentItems.map((resource, idx) => (
              <Col key={idx}>
                <PhysicalInstanceCard resource={resource} />
              </Col>
            ))}
          </Row>
        )}
        <Pagination className="justify-content-center" style={{ paddingTop: '30px' }}>
          {renderPaginationItems()}
        </Pagination>
      </Container>
    </div>
  );
}

export default Physical;
