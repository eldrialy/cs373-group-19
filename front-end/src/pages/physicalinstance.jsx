import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import Container from "react-bootstrap/Container";
import Spinner from "react-bootstrap/Spinner";
import { fetchPhysicalResourceById } from "../data/api";
import { Row, Col } from "react-bootstrap";
import styles from "./ModelStyle.module.css";
import LegalInstanceCard from '../components/LegalInstanceCard';
import EducationInstanceCard from '../components/EducationInstanceCard';

const PhysicalInstance = () => {
  const { id } = useParams();
  const [resource, setResource] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [educational, setEducational] = useState(null);
  const [legal, setLegal] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const currentResource = await fetchPhysicalResourceById(id);
        if (!currentResource) {
          throw new Error("Resource not found");
        }
        const educational = currentResource.associations.educational;
        const legal = currentResource.associations.legal;
        setEducational(educational);
        setLegal(legal);
        setResource(currentResource.resource);
      } catch (error) {
        setError(error.message);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [id]);

  if (loading) {
    return (
      <div className="text-center p-3">
        <Spinner animation="border" role="status">
          <span className="visually-hidden">Loading...</span>
        </Spinner>
      </div>
    );
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  if (!resource) {
    return <div>Resource not found</div>;
  }

  return (
    <>
      <Container>
        <Card border="primary" className="mx-auto">
          <Card.Img
            className="mx-auto d-block" style={{ height: 'auto', maxWidth: '50%' }}
            variant="top"
            src={
              resource.img_url
                ? resource.img_url
                : require("./../assets/educationimg.png")
            }
          />
          <Card.Body>
            <Card.Title>{resource.name}</Card.Title>
            <Card.Text>{resource.description}</Card.Text>
            <ListGroup variant="flush">
              <ListGroup.Item>Borough: {resource.borough}</ListGroup.Item>
              <ListGroup.Item>Address: {resource.address}</ListGroup.Item>
              <ListGroup.Item>
                Languages: {resource.language_svcs.join(", ")}
              </ListGroup.Item>
              <ListGroup.Item>
                Children-friendly: {resource.children_friendly ? "Yes" : "No"}
              </ListGroup.Item>
              <ListGroup.Item>
                LGBTQ-specialized: {resource.lgbtq_friendly ? "Yes" : "No"}
              </ListGroup.Item>
              <ListGroup.Item>
                Wheelchair-access: {resource.wheelchair_access ? "Yes" : "No"}
              </ListGroup.Item>
            </ListGroup>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              {resource.website && (
                <Card.Link href={resource.website}>
                  Click Here to Visit this Site
                </Card.Link>
              )}
            </div>
            <iframe
              width="450"
              height="250"
              frameBorder="0"
              style={{ border: "0" }}
              referrerPolicy="no-referrer-when-downgrade"
              src={resource.map_url}
              title={`${resource.name} map`}
              allowFullScreen
            ></iframe>
          </Card.Body>
        </Card>
      </Container>

      <Container>
        <header className={styles.titleContainer}>
          <h1 className={`text-center mb-4 display-6 ${styles.titleText}`}>
            Related Resources
          </h1>
        </header>
        <div>
          <Row>
            <Col sm={6}>
              <h4>Nearby education resource:</h4>
              <EducationInstanceCard resource={educational} />
            </Col>
            <Col sm={6}>
              <h4>Nearby legal resource:</h4>
              <LegalInstanceCard resource={legal} />
            </Col>
          </Row>
        </div>
      </Container>
    </>
  );
};

export default PhysicalInstance;
