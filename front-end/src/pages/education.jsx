import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Spinner from "react-bootstrap/Spinner";
import EducationInstanceCard from "../components/EducationInstanceCard";
import { fetchEducationalResources } from "../data/api";
import styles from "./ModelStyle.module.css";
import Pagination from "react-bootstrap/Pagination";
import GenericSearch from '../components/GenericSearch';
import { useNavigate } from "react-router-dom";
import Filtering from "../components/Filtering";

function Education() {
  const [educationResources, setEducationResources] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const instancesOnPage = 12;
  const [currentPage, setCurrentPage] = useState(1);
  const [error, setError] = useState(null);
  const [name_order, setNameOrder] = useState('');
  const [borough, setBorough] = useState('');
  const [faith_based, setFaithBased] = useState('');
  const [lgbtq_specialized, setLgbtqSpecialized] = useState('');
  const [child_friendly, setChildFriendly] = useState('');
  const [nonprofit, setNonprofit] = useState('');

  const navigate = useNavigate();

  const handleSearch = (query) => {
    navigate(`/searchedu?query=${query}`);
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);
        const resources = await fetchEducationalResources({
          name_order,
          borough,
          faith_based,
          lgbtq_specialized,
          child_friendly,
          nonprofit,
        });
        setEducationResources(resources);
      } catch (error) {
        setError(error.message);
        console.error("Failed to fetch educational resources:", error);
      } finally {
        setIsLoading(false);
      }
    };
    fetchData();
  }, [name_order, borough, nonprofit, lgbtq_specialized, child_friendly, faith_based]);

  if (error) {
    return <div>Error: {error}</div>;
  }

  const itemLength = educationResources.length;
  const pageLength = Math.ceil(itemLength / instancesOnPage);
  const indexOfLastItem = currentPage * instancesOnPage;
  const indexOfFirstItem = indexOfLastItem - instancesOnPage;
  const currentItems = educationResources.slice(
    indexOfFirstItem,
    indexOfLastItem
  );

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
    window.scrollTo(0, 0);
  };

  const renderPaginationItems = () => {
    let paginationItems = [];
    for (let number = 1; number <= pageLength; number++) {
      paginationItems.push(
        <Pagination.Item
          key={number}
          active={number === currentPage}
          onClick={() => handlePageChange(number)}
        >
          {number}
        </Pagination.Item>
      );
    }
    return paginationItems;
  };

  return (
    <div className="container my-5">
      <div style={{ display: 'flex', justifyContent: 'center' }}>
        <GenericSearch basePath="searchedu" placeholder="Search Education" onSearch={handleSearch} />
      </div>
      <header className={styles.titleContainer}>
        <h1 className={`text-center mb-4 display-6 ${styles.titleText}`}>
          Education Resources
        </h1>
        <p className="text-center text-white">
          Showing {indexOfFirstItem + 1}-{indexOfFirstItem + currentItems.length} of {itemLength} total resources
        </p>
      </header>
      <Filtering
        name_order={name_order}
        setNameOrder={setNameOrder}
        borough={borough}
        setBorough={setBorough}
        nonprofit={nonprofit}
        setNonprofit={setNonprofit}
        faith_based={faith_based}
        setFaithBased={setFaithBased}
        lgbtq_specialized={lgbtq_specialized}
        setLgbtqSpecialized={setLgbtqSpecialized}
        child_friendly={child_friendly}
        setChildFriendly={setChildFriendly}
      />
      <Pagination className="justify-content-center">
        {renderPaginationItems()}
      </Pagination>
      <Container>
        {isLoading ? (
          <div className="text-center p-3">
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          </div>
        ) : (
          <Row xs={1} md={3} className="g-4">
            {currentItems.map((resource, idx) => (
              <Col key={idx}>
                <EducationInstanceCard resource={resource} />
              </Col>
            ))}
          </Row>
        )}
        <Pagination className="justify-content-center" style={{ paddingTop: '30px' }}>
          {renderPaginationItems()}
        </Pagination>
      </Container>
    </div>
  );
}

export default Education;
