import React, { useEffect, useState } from "react";
import { fetchPhysicalResources } from "../data/api";
import Spinner from "react-bootstrap/Spinner";
import * as d3 from "d3";

const languages = [
  "English",
  "Spanish",
  "Haitian Creole",
  "Arabic",
  "American Sign Language (ASL)",
  "French",
  "Albanian",
  "Bengali",
  "Chinese",
  "Hindi",
  "Korean",
  "Polish",
  "Urdu",
  "Russian",
  "Other",
];

const colors = d3.schemeSet3;

const assignColors = (data) => {
  const assignedColors = [];
  let lastColorIndex = -1;

  data.forEach(() => {
    let colorIndex = (lastColorIndex + 1) % colors.length;
    while (colorIndex === lastColorIndex) {
      colorIndex = (colorIndex + 1) % colors.length;
    }
    assignedColors.push(colors[colorIndex]);
    lastColorIndex = colorIndex;
  });

  return assignedColors;
};

const getShortName = (language) => {
  const abbreviations = {
    "American Sign Language (ASL)": "ASL",
  };
  return abbreviations[language] || language;
};

const LanguageChart = () => {
  const [resourceCounts, setResourceCounts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [dimensions, setDimensions] = useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  const fetchResourcesByLanguage = async () => {
    setLoading(true);
    const counts = [];

    for (const language_svcs of languages) {
      const resources = await fetchPhysicalResources({ language_svcs });
      counts.push({ language_svcs, count: resources.length });
    }
    setResourceCounts(counts);
    setLoading(false);
  };

  useEffect(() => {
    fetchResourcesByLanguage();
  }, []);

  useEffect(() => {
    if (resourceCounts.length > 0) {
      drawPieChart();
    }
  }, [resourceCounts, dimensions]);

  useEffect(() => {
    const handleResize = () => {
      setDimensions({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const drawPieChart = () => {
    d3.select("#languagechart").select("svg").remove();
    const margin = { top: 20, right: 30, bottom: 40, left: 90 };
    const width = Math.min(dimensions.width, 800) - margin.left - margin.right;
    const height =
      Math.min(dimensions.height, 800) - margin.top - margin.bottom;
    const radius = Math.min(width, height) / 2;
    const svg = d3
      .select("#languagechart")
      .append("svg")
      .attr("width", width)
      .attr("height", height)
      .append("g")
      .attr("transform", `translate(${width / 2}, ${height / 2})`);

    const pie = d3.pie().value((d) => d.count)(resourceCounts);

    const arc = d3.arc().innerRadius(0).outerRadius(radius);

    const labelArc = d3
      .arc()
      .innerRadius(radius * 0.9)
      .outerRadius(radius * 0.9);

    const colorAssignment = assignColors(pie);

    svg
      .selectAll("path")
      .data(pie)
      .enter()
      .append("path")
      .attr("d", arc)
      .attr("fill", (d, i) => colorAssignment[i]);

    svg
      .selectAll("text")
      .data(pie)
      .enter()
      .append("text")
      .attr("transform", (d) => `translate(${labelArc.centroid(d)})`)
      .attr("dy", "0.35em")
      .attr("text-anchor", "middle")
      .text((d) => getShortName(d.data.language_svcs));
  };

  return (
    <div id="languagechart" className="d-inline-block align-center">
      {loading ? (
        <div className="text-center p-3">
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      ) : (
        resourceCounts.length > 0 && drawPieChart()
      )}
    </div>
  );
};

export default LanguageChart;
