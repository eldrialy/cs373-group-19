import React, { useEffect, useState } from "react";
import Spinner from "react-bootstrap/Spinner";
import axios from "axios";
import * as d3 from "d3";

const url = 'https://d1ahbxgizovdow.cloudfront.net/county';

const counties = ["Alameda", "Alpine", "Amador",
                  "Berkeley", "Butte",
                  "Calaveras", "Colusa", "Contra Costa",
                  "Del Norte", "El Dorado",
                  "Fresno", "Glenn",
                  "Humboldt", "Imperial", "Inyo",
                  "Kern", "Kings",
                  "Lake", "Lassen", "Los Angeles",
                  "Madera", "Marin", "Mariposa",
                  "Mendocino", "Merced", "Modoc",
                  "Mono", "Monterey",
                  "Napa", "Nevada",
                  "Orange", "Pasadena",
                  "Placer", "Plumas", "Riverside",
                  "Sacramento", "San Bernardino",
                  "San Diego", "San Francisco",
                  "San Joaquin", "San Luis Obispo",
                  "San Mateo", "Santa Barbara",
                  "Santa Clara", "Santa Cruz",
                  "Shasta", "Sierra",
                  "Siskiyou", "Solano",
                  "Sonoma", "Stanislaus", "Sutter",
                  "Tehama", "Trinity", "Tulare", "Tuolumne",
                  "Ventura", "Yolo", "Yuba"
                ];

const colors = d3.schemeSet3;

const assignColors = (data) => {
  const assignedColors = [];
  let lastColorIndex = -1;

  data.forEach(() => {
    let colorIndex = (lastColorIndex + 1) % colors.length;
    while (colorIndex === lastColorIndex) {
      colorIndex = (colorIndex + 1) % colors.length;
    }
    assignedColors.push(colors[colorIndex]);
    lastColorIndex = colorIndex;
  });

  return assignedColors;
};


const CountyChart = () => {
  const [countyPrevalence, setCountyPrevalence] = useState([]);
  const [loading, setLoading] = useState(false);
  const [dimensions, setDimensions] = useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  const fetchPrevalenceByCounty = async () => {
    setLoading(true);
    //const countyData = [];
    // for (const county of counties) {
    const response = await axios.get(url);
    //   countyData.push({ county: county.name, prev: parseInt(county.ccases) });
    // }
    
    const countyData = response.data.rows.map((row) => ({
      county: row.name,
      cases: parseInt(row.scases),
    }));
    setCountyPrevalence(countyData);
    setLoading(false);
  };


  useEffect(() => {
    fetchPrevalenceByCounty();
  }, []);

  useEffect(() => {
    if (countyPrevalence.length > 100) {
      drawPieChart();
    }
  }, [countyPrevalence, dimensions]);

  useEffect(() => {
    const handleResize = () => {
      setDimensions({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const drawPieChart = () => {
    d3.select("#countychart").select("svg").remove();
    const margin = { top: 100, right: 100, bottom: 30, left: 90 };
    const width = Math.min(dimensions.width, 800) - margin.left - margin.right;
    const height =
      Math.min(dimensions.height, 800) - margin.top - margin.bottom;
    const radius = Math.min(width, height) / 2;
    const svg = d3
      .select("#countychart")
      .append("svg")
      .attr("width", width)
      .attr("height", height)
      .append("g")
      .attr("transform", `translate(${width / 2}, ${height / 2})`);

    const pie = d3.pie().value((d) => d.cases)(countyPrevalence);

    const arc = d3.arc().innerRadius(0).outerRadius(radius);

    const labelArc = d3
      .arc()
      .innerRadius(radius)
      .outerRadius(radius * .5);

    const colorAssignment = assignColors(pie);

    svg
      .selectAll("path")
      .data(pie)
      .enter()
      .append("path")
      .attr("d", arc)
      .attr("fill", (d, i) => colorAssignment[i]);

    svg
      .selectAll("text")
      .data(pie)
      .enter()
      .append("text")
      .attr("transform", (d) => `translate(${labelArc.centroid(d)})`)
      .attr("dx", "-1.0em")
      .attr("dy", "0.35em")
      .attr("transform", function(d) { 
        var midAngle = d.endAngle < Math.PI ? d.startAngle/2 + d.endAngle/2 : d.startAngle/2  + d.endAngle/2 + Math.PI ;
        return "translate(" + labelArc.centroid(d)[0] + "," + labelArc.centroid(d)[1] + ") rotate(-90) rotate(" + (midAngle * 180/Math.PI) + ")"; })
      .attr('text-anchor','middle')
          .style("font-size", "12px")
      // .attr("transform", function(d) {
      //   var angle = (d.startAngle + d.endAngle) / 2;
      //   var centroid = labelArc.centroid(d);
      //   var x = Math.cos(angle) * (radius + 20);
      //   var y = Math.sin(angle) * (radius + 20);
      //   return 'translate(${x}, ${y})';
      // })
      .text((d) => d.data.county);
  };

  return (
    <div id="countychart" className="d-inline-block align-center">
      {loading ? (
        <div className="text-center p-3">
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      ) : (
        countyPrevalence.length > 0 && drawPieChart()
      )}
    </div>
  );
};


export default CountyChart;