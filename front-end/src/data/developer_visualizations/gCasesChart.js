import React, { useEffect, useState } from "react";
import Spinner from "react-bootstrap/Spinner";
import * as d3 from "d3";
import axios from "axios";

const url = 'https://d1ahbxgizovdow.cloudfront.net/county';

const counties = ["Alameda", "Alpine", "Amador",
                  "Berkeley", "Butte",
                  "Calaveras", "Colusa", "Contra Costa",
                  "Del Norte", "El Dorado",
                  "Fresno", "Glenn",
                  "Humboldt", "Imperial", "Inyo",
                  "Kern", "Kings",
                  "Lake", "Lassen", "Los Angeles",
                  "Madera", "Marin", "Mariposa",
                  "Mendocino", "Merced", "Modoc",
                  "Mono", "Monterey",
                  "Napa", "Nevada",
                  "Orange", "Pasadena",
                  "Placer", "Plumas", "Riverside",
                  "Sacramento", "San Bernardino",
                  "San Diego", "San Francisco",
                  "San Joaquin", "San Luis Obispo",
                  "San Mateo", "Santa Barbara",
                  "Santa Clara", "Santa Cruz",
                  "Shasta", "Sierra",
                  "Siskiyou", "Solano",
                  "Sonoma", "Stanislaus", "Sutter",
                  "Tehama", "Trinity", "Tulare", "Tuolumne",
                  "Ventura", "Yolo", "Yuba"
                ];

const GCasesChart = () => {
  const [gCaseCounts, setGCaseCounts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [dimensions, setDimensions] = useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  const fetchData = async () => {
    setLoading(true);
    const response = await axios.get(url);
    const counts = response.data.rows.map((row) => ({
      name: row.name,
      count: parseInt(row.ccases),
    }));
    setGCaseCounts(counts);
    setLoading(false);
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    if (gCaseCounts.length > 0) {
      drawChart();
    }
  }, [gCaseCounts, dimensions]);

  useEffect(() => {
    const handleResize = () => {
      setDimensions({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const drawChart = () => {
    d3.select("#gcasechart").select("svg").remove();

    const margin = { top: 30, right: 30, bottom: 140, left: 50 };
    const totalWidth = Math.min(dimensions.width, 800);
    const totalHeight = Math.min(dimensions.height, 500);
    const width = totalWidth - margin.left - margin.right;
    const height = totalHeight - margin.top - margin.bottom;

    const svg = d3
      .select("#gcasechart")
      .append("svg")
      .attr("width", totalWidth)
      .attr("height", totalHeight)
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);

    const x = d3.scaleBand().range([0, width]).padding(0.1);
    const y = d3.scaleLinear().range([height, 0]);

    const color = d3.scaleOrdinal(d3.schemeSet2);

    x.domain(gCaseCounts.map((r) => r.name));
    y.domain([0, d3.max(gCaseCounts, (r) => r.count)]);

    svg
      .selectAll(".bar")
      .data(gCaseCounts)
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("x", (r) => x(r.name))
      .attr("y", (r) => y(r.count))
      .attr("width", x.bandwidth())
      .attr("height", (r) => height - y(r.count))
      .attr("fill", (r) => color(r.name));

    svg.append("g");
    svg
      .append("g")
      .attr("transform", `translate(0,${height})`)
      .call(d3.axisBottom(x))
      .selectAll("text")
      .style("font-size", "14px")
      .style("text-anchor", "end")
      .attr("dx", "-1.0em")
      .attr("dy", ".10em")
      .attr("transform", "rotate(-75)");

    svg
      .append("g")
      .call(d3.axisLeft(y))
      .selectAll("text")
      .style("font-size", "16px");
  };

  return (
    <div id="gcasechart" className="d-inline-block align-center">
      {loading ? (
        <div className="text-center p-3">
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      ) : (
        gCaseCounts.length > 0 && drawChart()
      )}
    </div>
  );
};

export default GCasesChart;
