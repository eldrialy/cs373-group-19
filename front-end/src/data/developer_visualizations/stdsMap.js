import React, { useEffect, useState } from "react";
import Spinner from "react-bootstrap/Spinner";
import axios from "axios";
import * as d3 from "d3";

const url = 'https://d1ahbxgizovdow.cloudfront.net/prevalence';

const years = ["2001", "2002", "2003", "2004", "2005",
               "2006", "2007", "2008", "2009", "2010",
               "2011", "2012", "2013", "2014", "2015",
               "2016", "2017", "2018", "2019", "2020", "2021"];
const stds = ["Chlamydia", "Gonorrhea", "Syphilis"];

const STDMapChart = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);

      const response = await axios.get(url);

      const transformedData = response.data.rows.reduce((years, row) => {
        if (row.sex === "Total") {
          const year = row.year;
          const Chlamydia = parseInt(row.c_cases);
          const Gonorrhea = parseInt(row.g_cases);
          const Syphilis = parseInt(row.s_cases);
          if (!years[year]) {
            years[year] = { year, Chlamydia: 0, Gonorrhea: 0, Syphilis: 0 };
          }
          years[year].Chlamydia += Chlamydia;
          years[year].Gonorrhea += Gonorrhea;
          years[year].Syphilis += Syphilis;
         }
        return years;
      }, {});
      setData(Object.values(transformedData));
      setLoading(false);
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (!loading) {
      drawSTDMap();
    }
  }, [data, loading]);

  const drawSTDMap = () => {
    d3.select("#stdmap").select("svg").remove();

    const margin = { top: 30, right: 10, bottom: 70, left: 60 },
      width = 600 - margin.left - margin.right,
      height = 450 - margin.top - margin.bottom;

    const svg = d3.select("#stdmap")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);

    const myGroups = years;
    const myVarNames = stds;

    const x = d3.scaleBand()
      .range([0, width])
      .domain(myGroups)
      .padding(0.05);
    svg.append("g")
      .attr("transform", `translate(0, ${height})`)
      .call(d3.axisBottom(x))

    const y = d3.scaleBand()
      .range([height, 0])
      .domain(myVarNames)
      .padding(0.05);
    svg.append("g")
      .call(d3.axisLeft(y));

    // color scale
    const myColor = d3.scaleSequential( 
      [0, 200000],
      d3.interpolateReds
      );  
      
    const tooltip = d3.select("#stdmap")
      .append("div")
      .style("opacity", 0)
      .attr("class", "tooltip")
      .style("background-color", "white")
      .style("border", "solid")
      .style("border-width", "2px")
      .style("border-radius", "5px")
      .style("padding", "5px")

    const mouseover = function (event, d) {
      tooltip.style("opacity", 1)
    }
    const mousemove = function (event, d) {
      tooltip
        .html(`The exact value of<br>this cell is: ${d.value}`)
        .style("left", `${event.x}px`)
        .style("top", `${event.y}px`)
    }
    const mouseleave = function (event, d) {
      tooltip.style("opacity", 0)
    }


      data.forEach(function(groupData) {
        myVarNames.forEach(function(varName) {

        const cell = svg.append("g");
        cell
            .append("rect")
            .attr("x", x(groupData.year))
            .attr("y", y(varName))
            .attr("rx", 4)
            .attr("ry", 4)
            .attr("width", x.bandwidth())
            .attr("height", y.bandwidth())
            .style("fill", myColor(groupData[varName]))
            .style("stroke-width", 4)
            .style("stroke", "none")
            .style("opacity", 0.8)
            .on("mouseover", mouseover)
            .on("mousemove", function(event) {
            const dataValue = d3.select(this).datum()[varName];
            tooltip
                .html(`The exact value of<br>this cell is: ${dataValue}`)
                .style("left", (event.pageX + 10) + "px") 
                .style("top", (event.pageY + 10) + "px") 
                .style("opacity", 1);
            })
            .on("mouseleave", mouseleave)
            .datum(groupData); 
        })
      })

  };

  return (
    <div id="stdmap">
      {loading ? <Spinner animation="border" /> : null}
    </div>
  );
};

export default STDMapChart;