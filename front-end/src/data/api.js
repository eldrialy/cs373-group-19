import axios from "axios";

const apiURL = "http://127.0.0.1:5000";

export const fetchEducationalResources = async (filters) => {
  try {
    let queryString = '';
    if (filters) {
      const queryParams = new URLSearchParams(filters).toString()
      queryString = `?${queryParams}`;
    }
    const response = await axios.get(`${apiURL}/educational/collection${queryString}`);
    console.log("data", response.data);
    return response.data.resources;
  } catch (error) {
    console.error("Failed to fetch educational resources:", error);
    return [];
  }
};

export const fetchEducationalResourceById = async (id) => {
  try {
    const response = await axios.get(`${apiURL}/educational/resource?id=${id}`);
    return response.data;
  } catch (error) {
    console.error(`Failed to fetch educational resource with ID ${id}:`, error);
    return null;
  }
};

export const fetchLegalResources = async (filters) => {
  try {
    let queryString = '';
    if (filters) {
      const queryParams = new URLSearchParams(filters).toString()
      queryString = `?${queryParams}`;
    }
    const response = await axios.get(`${apiURL}/legal/collection${queryString}`);
    console.log("data", response.data);
    return response.data.resources;
  } catch (error) {
    console.error("Failed to fetch legal resources:", error);
    return [];
  }
};

export const fetchLegalResourceById = async (id) => {
  try {
    const response = await axios.get(`${apiURL}/legal/resource?id=${id}`);
    return response.data;
  } catch (error) {
    console.error(`Failed to fetch legal resource with ID ${id}:`, error);
    return null;
  }
};

export const fetchPhysicalResources = async (filters) => {
  try {
    let queryString = '';
    if (filters) {
      const queryParams = new URLSearchParams(filters).toString()
      queryString = `?${queryParams}`;
    }
    const response = await axios.get(`${apiURL}/physical/collection${queryString}`);
    console.log("data", response.data);
    return response.data.resources;
  } catch (error) {
    console.error("Failed to fetch physical resources:", error);
    return [];
  }
};

export const fetchPhysicalResourceById = async (id) => {
  try {
    const response = await axios.get(`${apiURL}/physical/resource?id=${id}`);
    return response.data;
  } catch (error) {
    console.error(`Failed to fetch physical resource with ID ${id}:`, error);
    return null;
  }
};