const testimonials = [
    {
      id: 1,
      name: "Demi Moore",
      quote: "As a survivor of domestic abuse, each day brings a sense of peace and healing. To those of you reading who have been victims of domestic abuse, to those who are victims now, and especially to those who will become a victim of domestic abuse, I promise you there is hope. You chose to read this today for a reason.",
      link: "https://dapdc.org/survivor_stories/survivor-story-demi-moore/",
    },
    {
      id: 2,
      name: "Colleen",
      quote: "I was in an abusive relationship for 2 years from 2018-2020 where I was assaulted countless of times. I had never experienced domestic violence before and had a hard time registering it in my head as abuse during the relationship from the constant brain wash, gaslighting and manipulation tactics he used. I felt a sense of pity on him due to his past traumas, which also played a huge role as well. Every time he assaulted me.. I lost myself more and more where eventually I lost myself completely. After I finally left, it took months for me to stop defending and protecting my ex and his behaviour, and to accept that what I went through was extreme abuse. I started educating myself on the cycle of abuse and realized I was in a deep trauma bond and needed to break it.",
      link: "https://breakthesilencedv.org/my-story-colleen/",
    },
    {
      id: 3,
      name: "Ricci",
      quote: "My childhood story is, unfortunately, not unusual. In fact there are many battered men, women and children in the world who suffer in silence every day. But those victims of domestic violence are ones that the general public understands. If I were to share the horrific stories of my grandmother being beat up by my alcoholic grandfather, the story of my mother's nose broken by her angry husband or even the sexual abuse of a childhood friend, most would agree that they too have experienced something similar or have stories of their own friends and family members who have suffered the same. However, I am here to talk about being a victim of another kind of domestic violence -- the kind that has no face.",
      link: "https://www.seattle.gov/cityattorney/crime-victim-assistance/domestic-violence-help/to-hell-and-back-a-survivors-story",
    }
  ];

export default testimonials;