import React, { useEffect, useState } from "react";
import * as d3 from "d3";
import { fetchEducationalResources, fetchLegalResources, fetchPhysicalResources } from "../data/api";
import Spinner from "react-bootstrap/Spinner";

const boroughs = ["Manhattan", "Brooklyn", "Queens", "Bronx", "Staten Island"];
const categories = ["Educational", "Legal", "Physical"];

const HeatMapChart = () => {
  const [loading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);

      const educationalResources = await fetchEducationalResources();
      const legalResources = await fetchLegalResources();
      const physicalResources = await fetchPhysicalResources();

      const transformedData = boroughs.map(borough => ({
        borough,
        Educational: educationalResources.filter(r => r.borough === borough).length,
        Legal: legalResources.filter(r => r.borough === borough).length,
        Physical: physicalResources.filter(r => r.borough === borough).length
      }));

      setData(transformedData);
      setLoading(false);
    };

    fetchData();
  }, []);

  useEffect(() => {
    if (!loading) {
      drawHeatMap();
    }
  }, [data, loading]);

  const drawHeatMap = () => {
    d3.select("#heatmap").select("svg").remove();

    const margin = { top: 30, right: 30, bottom: 70, left: 60 },
      width = 460 - margin.left - margin.right,
      height = 400 - margin.top - margin.bottom;

    const svg = d3.select("#heatmap")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);

    const myGroups = boroughs;
    const myVars = categories;

    const x = d3.scaleBand()
      .range([0, width])
      .domain(myGroups)
      .padding(0.05);
    svg.append("g")
      .attr("transform", `translate(0, ${height})`)
      .call(d3.axisBottom(x))

    const y = d3.scaleBand()
      .range([height, 0])
      .domain(myVars)
      .padding(0.05);
    svg.append("g")
      .call(d3.axisLeft(y));

    // color scale
    const myColor = d3.scaleSequential()
      .interpolator(d3.interpolateGreens)
      .domain([1, 70]) 

    const tooltip = d3.select("#heatmap")
      .append("div")
      .style("opacity", 0)
      .attr("class", "tooltip")
      .style("background-color", "white")
      .style("border", "solid")
      .style("border-width", "2px")
      .style("border-radius", "5px")
      .style("padding", "5px")

    const mouseover = function (event, d) {
      tooltip.style("opacity", 1)
    }
    const mousemove = function (event, d) {
      tooltip
        .html(`The exact value of<br>this cell is: ${d.value}`)
        .style("left", `${event.x}px`)
        .style("top", `${event.y}px`)
    }
    const mouseleave = function (event, d) {
      tooltip.style("opacity", 0)
    }

    data.forEach(function(groupData) {
        myVars.forEach(function(varName) {
        const cell = svg.append("g");
        cell
            .append("rect")
            .attr("x", x(groupData.borough))
            .attr("y", y(varName))
            .attr("rx", 4)
            .attr("ry", 4)
            .attr("width", x.bandwidth())
            .attr("height", y.bandwidth())
            .style("fill", myColor(groupData[varName]))
            .style("stroke-width", 4)
            .style("stroke", "none")
            .style("opacity", 0.8)
            .on("mouseover", mouseover)
            .on("mousemove", function(event) {
            const dataValue = d3.select(this).datum()[varName];
            tooltip
                .html(`The exact value of<br>this cell is: ${dataValue}`)
                .style("left", (event.pageX + 10) + "px") 
                .style("top", (event.pageY + 10) + "px") 
                .style("opacity", 1);
            })
            .on("mouseleave", mouseleave)
            .datum(groupData); 
        })
    })

  };

  return (
    <div id="heatmap">
      {loading ? <Spinner animation="border" /> : null}
    </div>
  );
};

export default HeatMapChart;