import axios from "axios";

const gitLabAPI = "https://gitlab.com/api/v4";
const accessToken = "glpat-dBDadMkrTa_fgWQ4Ejdf";
const projectId = "54553166";

const config = {
  headers: { "PRIVATE-TOKEN": accessToken },
  params: { per_page: 100},
};

export const fetchCommits = async () => {
  try {
    const response = await axios.get(
      `${gitLabAPI}/projects/${projectId}/repository/contributors`,
      { ...config }
    );

    return response.data;
  } catch (error) {
    console.error("Failed to fetch commits:", error);
    return [];
  }
};

export const fetchIssues = async () => {
  try {
    let allIssues = [];
    let page = 1;
    let response = await axios.get(
      `${gitLabAPI}/projects/${projectId}/issues`,
      { ...config, params: { ...config.params, page } }
    );

    while (response.data.length > 0) {
      allIssues.push(...response.data);
      page++;
      response = await axios.get(
        `${gitLabAPI}/projects/${projectId}/issues`,
        { ...config, params: { ...config.params, page } }
      );
    }

    return allIssues;
  } catch (error) {
    console.error("Failed to fetch issues:", error);
    return [];
  }
};