import React, { useEffect, useState } from "react";
import { fetchPhysicalResources } from "../data/api";
import { fetchLegalResources } from "../data/api";
import { fetchEducationalResources } from "../data/api";
import Spinner from "react-bootstrap/Spinner";
import * as d3 from "d3";

const boroughs = ["Manhattan", "Brooklyn", "Queens", "Bronx", "Staten Island"];

const BoroughChart = () => {
  const [resourceCounts, setResourceCounts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [dimensions, setDimensions] = useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });

  const fetchResourcesByBorough = async () => {
    setLoading(true);
    const counts = [];

    for (const borough of boroughs) {
      const physical = await fetchPhysicalResources({ borough });
      let count = physical.length;
    
      const legal = await fetchLegalResources({ borough });
      count += legal.length;
    
      const educational = await fetchEducationalResources({ borough });
      count += educational.length;
    
      counts.push({
        borough,
        count
      });
    }
    setResourceCounts(counts);
    setLoading(false);
  };

  useEffect(() => {
    fetchResourcesByBorough();
  }, []);

  useEffect(() => {
    if (resourceCounts.length > 0) {
      drawChart();
    }
  }, [resourceCounts, dimensions]);

  useEffect(() => {
    const handleResize = () => {
      setDimensions({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };

    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const drawChart = () => {
    d3.select("#boroughchart").select("svg").remove();

    const margin = { top: 20, right: 30, bottom: 40, left: 90 };
    const totalWidth = Math.min(dimensions.width, 800);
    const totalHeight = Math.min(dimensions.height, 500);
    const width = totalWidth - margin.left - margin.right;
    const height = totalHeight - margin.top - margin.bottom;

    const svg = d3
      .select("#boroughchart")
      .append("svg")
      .attr("width", totalWidth)
      .attr("height", totalHeight)
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);

    const x = d3.scaleBand().range([0, width]).padding(0.1);
    const y = d3.scaleLinear().range([height, 0]);

    const color = d3.scaleOrdinal(d3.schemeSet2);

    x.domain(resourceCounts.map((r) => r.borough));
    y.domain([0, d3.max(resourceCounts, (r) => r.count)]);

    svg
      .selectAll(".bar")
      .data(resourceCounts)
      .enter()
      .append("rect")
      .attr("class", "bar")
      .attr("x", (r) => x(r.borough))
      .attr("y", (r) => y(r.count))
      .attr("width", x.bandwidth())
      .attr("height", (r) => height - y(r.count))
      .attr("fill", (r) => color(r.borough));

    svg.append("g");
    svg
      .append("g")
      .attr("transform", `translate(0,${height})`)
      .call(d3.axisBottom(x))
      .selectAll("text")
      .style("font-size", "16px");

    svg
      .append("g")
      .call(d3.axisLeft(y))
      .selectAll("text")
      .style("font-size", "16px");
  };

  return (
    <div id="boroughchart" className="d-inline-block align-center">
      {loading ? (
        <div className="text-center p-3">
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      ) : (
        resourceCounts.length > 0 && drawChart()
      )}
    </div>
  );
};
export default BoroughChart;
