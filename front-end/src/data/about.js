import ava from "./../assets/ava.png";
import jamie from "./../assets/jamie.png";
import alyssa from "./../assets/alyssa.png";
import francis from "./../assets/francis.png";
import pj from "./../assets/pj.png";
import amplify from "./../assets/amplify.svg";
import gitlab from "./../assets/gitlab.svg";
import react from "./../assets/react.png";
import postman from "./../assets/postman.svg";
import ecs from "./../assets/ecs.png";
import supabase from "./../assets/supabase.png";

export const team = [
  {
    name: "Jamie Babbe",
    gitlab_id: "jamiebabbe",
    email: ["jbabbe@cs.utexas.edu", "jamiebabbe@utexas.edu", "jamiebabbe@Jamies-MacBook-Air.local", "jamiebabbe@wireless-10-155-182-122.public.utexas.edu"],
    role: "Frontend",
    bio: "I am a 3rd year CS major at UT Austin. I enjoy most sports, but mainly volleyball!",
    tests: 0,
    image: jamie,
  },
  {
    name: "Alyssa Eldridge",
    gitlab_id: "eldrialy",
    email: ["eldrialy@utexas.edu"],
    role: "Frontend",
    bio: "I am a 3rd year CS major at UT Austin. In addition to programming, I enjoy crafts and puzzles.",
    tests: 10,
    image: alyssa,
  },
  {
    name: "Ava Machado",
    gitlab_id: "avamachado",
    email: ["avamachado@utexas.edu"],
    role: "Backend",
    bio: "I'm a senior CS major at UT Austin. I enjoy rock climbing and pickleball.",
    tests: 0,
    image: ava,
  },
  {
    name: "PJ Shenoy",
    gitlab_id: "shenoyprath0",
    email: ["46547158+shenoyprath@users.noreply.github.com", "shenoyprath@protonmail.com"],
    role: "Backend",
    bio: "I'm a senior CS major at the University of Texas at Austin. In addition to coding, I enjoy flying airplanes.",
    tests: 12,
    image: pj,
  },
  {
    name: "Francis Yeh",
    gitlab_id: "francis.yeh",
    email: ["francis.yeh@hotmail.com"],
    role: "Frontend/Backend",
    bio: "I am a 3rd year CS Major at the University of Texas at Austin. I enjoy traveling and watching basketball!",
    tests: 0,
    image: francis,
  },
];

export const data_sources = [
  {
    name: "NYC Women's Resource Network Database",
    link: "https://data.cityofnewyork.us/Social-Services/NYC-Women-s-Resource-Network-Database/pqg4-dm6b/data",
    description: "Data scraped using REST API calls",
  },
  {
    name: "Mayor's Office to End Domestic and Gender-Based Violence Resource Directory",
    link: "https://data.cityofnewyork.us/Social-Services/Mayor-s-Office-to-End-Domestic-and-Gender-Based-Vi/5ziv-wcy4/about_data",
    description: "Data scraped using REST API calls",
  },
  {
    name: "Google Maps API",
    link: "https://developers.google.com/maps",
    description: "Data scraped using REST API calls",
  }
];

export const tools = [
  {
    name: "AWS Amplify",
    link: "https://aws.amazon.com/amplify/",
    use: "Used for hosting the website",
    logo: amplify,
  },
  {
    name: "React",
    link: "https://react.dev",
    use: "Used for building user interfaces",
    logo: react,
  },
  {
    name: "GitLab",
    link: "https://gitlab.com",
    use: "Provides repository and continuous integration",
    logo: gitlab,
  },
  {
    name: "Postman",
    link: "https://www.postman.com",
    use: "Facilitates API testing and development",
    logo: postman,
  },
  {
    name: "AWS ECS",
    link: "https://aws.amazon.com/ecs/",
    use: "Used for hosting back end",
    logo: ecs,
  },
  {
    name: "Supabase",
    link: "https://supabase.com",
    use: "Hosts PostgreSQL database",
    logo: supabase,
  }
];
