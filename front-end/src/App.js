import './App.css';
import About from './pages/about';
import Education from './pages/education'; 
import Legal from './pages/legal'; 
import Physical from './pages/physical'; 
import Splash from './pages/splash';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import NavigationBar from './components/NavigationBar';
import LegalInstance from './pages/legalinstance';
import PhysicalInstance from './pages/physicalinstance';
import EducationInstance from './pages/educationinstance';
import SearchPageResult from './components/SearchPageResult';
import SearchEduPageResult from './components/SearchEduPageResult';
import SearchLegalPageResult from './components/SearchLegalPageResult';
import SearchPhysPageResult from './components/SearchPhysPageResult';
import Visualizations from './pages/visualizations';
import ProviderVisualizations from './pages/providervisualizations';

function App() {
  return (
    <BrowserRouter>
      <NavigationBar />
      <Routes>
        <Route path="/about" element={<About />} />
        <Route path="/education" element={<Education />} /> 
        <Route path="/legal" element={<Legal />} /> 
        <Route path="/physical" element={<Physical />} /> 
        <Route path="/" element={<Splash />} />
        <Route path="/legalinstance/:id" element={<LegalInstance/>}/>
        <Route path="/physicalinstance/:id" element={<PhysicalInstance/>}/>
        <Route path="/educationinstance/:id" element={<EducationInstance/>}/>
        <Route path="/search" element={<SearchPageResult />} />
        <Route path="/searchedu" element={<SearchEduPageResult />} />
        <Route path="/searchlegal" element={<SearchLegalPageResult />} />
        <Route path="/searchphys" element={<SearchPhysPageResult />} />
        <Route path="/visualizations" element={<Visualizations />} />
        <Route path="/providervisualizations" element={<ProviderVisualizations />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
