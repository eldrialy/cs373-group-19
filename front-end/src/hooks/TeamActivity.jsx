import { useState, useEffect } from 'react';
import { fetchCommits, fetchIssues } from "../data/gitlab";

const useTeamActivity = (team) => {
  const [memberActivity, setMemberActivity] = useState({});
  const [totalStats, setTotalStats] = useState({
    commits: 0,
    issues: 0,
    tests: team.reduce((total, member) => total + member.tests, 0),
  });

  useEffect(() => {
    async function aggregateActivity() {
      const commits = await fetchCommits();
      const issues = await fetchIssues();

      const activityData = {};
      let totalCommits = 0;
      let totalIssues = 0;

      team.forEach(member => {
        activityData[member.name] = { commits: 0, issues: 0 };
      });

      commits.forEach(commit => {
        const member = team.find(member => member.email.some(email => email === commit.email));
        if (member) {
          activityData[member.name].commits += commit.commits;
          totalCommits += commit.commits;
        }
      });

      issues.forEach(issue => {
        if (issue.closed_by) {
          const member = team.find(member => member.gitlab_id === issue.closed_by.username);
          if (member) {
            activityData[member.name].issues += 1;
            totalIssues += 1;
          }
        }
      });

      setMemberActivity(activityData);
      setTotalStats(stats => ({ ...stats, commits: totalCommits, issues: totalIssues }));
    }

    aggregateActivity();
  }, [team]);

  return { memberActivity, totalStats };
};

export default useTeamActivity;
