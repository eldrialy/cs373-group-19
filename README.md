# cs373-group-19

### Phase 4

**Group Number:** 19

**Project Name:** NYC Survivor Support

**Team Member Information:**

| Name            | GitLab ID     | EID      | Estimated Completion Time | Actual Completion Time |
| :-------------- | ------------- | -------- | ------------------------- | ---------------------- |
| Jamie Babbe     | @jamiebabbe   | jmb22558 | 13                        | 12                       |
| Alyssa Eldridge | @eldrialy     | aje2273  | 20                        | 21                       |
| Ava Machado     | @avamachado   | acm4845  | 10                        | 12                       |                    |
| Francis Yeh     | @francis.yeh  | fy2585   | 15                        | 16                       |

**Phase Leader:** Jamie Babbe

**Responsibilities of Leader:**
Delegate tasks to members of the team and organize meetings. Set up configuration of project and ensure all tasks are done before submission. Communicate between developers and foster productive team meetings and discussion.

**GitLab Pipelines:**
https://gitlab.com/eldrialy/cs373-group-19/-/pipelines

**Git SHA:** 71897a0558847c5d83eebdb70d034db4c3394089

**Website Link:**
https://nycsurvivorsupport.me

**Youtube Link:**
https://youtu.be/VNPBFa_2OHU 

**API Link:**
https://api.nycsurvivorsupport.me

**API Documentation:**
https://documenter.getpostman.com/view/32924129/2sA3Bn5CNu

### Proposal

**Proposed Project:**   Our website serves to support and empower victims of domestic
violence living in New York City, NY by providing educational, legal, and physical
resources. NYC Survivor Support primarily aims to assist our users by guiding survivors
through the complexities of the legal system, offering immediate physical support through
shelters and counseling services, and fostering awareness and prevention through
educational initiatives and statistics. We are dedicated to creating a site that not
only promotes a safe healing process for survivors, but also equips others with the
skills and knowledge needed to offer effective support to victims in their life.

**Data Sources:**

1. https://data.cityofnewyork.us/resource/5ziv-wcy4.json (REST API)
2. https://data.cityofnewyork.us/resource/pqg4-dm6b.json (REST API)
3. https://developers.google.com/maps (REST API)

**Models:**

1. Physical Resources
2. Educational Resources
3. Legal Resources

**Estimated of Number of Instances:**

**Model Attributes:**

1. Physical Resources  (158 instances)
   - alphabetical
   - location
   - languages
   - wheelchair access
   - child friendly
   - LGBTQ+ specialized
2. Educational Resources  (119 instances)
   - alphabetical
   - location
   - languages
   - LGBTQ+ specialized
   - faith-based
   - child friendly
   - non profit
3. Legal Resources  (72 instances)
   - alphabetical
   - location
   - languages
   - LGBTQ+ specialized
   - faith-based
   - child friendly
   - non profit

**Connecting Models:**
All models connect to the other two models based on location which is calculated via longitude and latitude. The closest resource from each other model will be linked. For instance, educational will link to the closest physical and legal resources. Legal will link to the closest physical and educational resources. Educational will link to the closest physical and legal resources.

**Media Types:**

1. Education resources
   - map
   - images
   - text
2. Legal resources
   - map
   - images
   - text
3. Physical resources
   - maps
   - images
   - text

**Questions Our Site Answers:**

1. Where can I go in NYC to recieve education on domestic violence?
2. Where can I receive government legal assistance as a DV victim in NYC?
3. Where can I go in Brooklyn to find shelters for DV victims?
